			
		function approvalWeb(nodeList,tabList,flowStatus){
			//完整流程表格渲染
			var status = '';
			var flowName = '';
			var role = '';
			var status1 = '';
			if(flowStatus.flowStatus == 0){
		      	$('.tab-title a').html('未完成');
		    }else{
		      	$('.tab-title a').html('已完成');
		    }
			
			if(nodeList[0].nodeStatus == 1) {
				status1 = '未审批';
			} else if(nodeList[0].nodeStatus == 2) {
				status1 = '审批中';
			} else if(nodeList[0].nodeStatus == 3) {
				status1 = '已审批';
			}
			role1 = nodeList[0].nodeName.substr(2);
			var str1 = '<tr class="table_tr">' +
				'<td style="width:100px;">' + 1 + '</td>' +
				'<td style="width:150px;">' + nodeList[0].nodeName + '</td>' +
				'<td style="width:330px;">' + role1 + '</td>' +
				'<td>' + nodeList[0].nodeActor[0].actorName + '</td>' +
				'<td>' + status1 + '</td>' +
				'</tr>';
			$('#tabList').append(str1);
			for(var i = 1; i < nodeList.length; i++) {
				if(nodeList[i].nodeStatus == 1) {
					status = '未审批';
				} else if(nodeList[i].nodeStatus == 2) {
					status = '审批中';
				} else if(nodeList[i].nodeStatus == 3) {
					status = '已审批';
				}
				var strNode = nodeList[i].nodeName
				var flowNum = strNode.lastIndexOf('(');
				var roleNum = strNode.lastIndexOf(')');
				
				flowName = strNode.substr(0, flowNum);
				role = strNode.substr(flowNum+1, roleNum-flowNum-1);
				//role.replaceAll(", ,",",");
				var str = '<tr class="table_tr">' +
					'<td style="width:100px;">' + (i+1) + '</td>' +
					'<td style="width:150px;">' + flowName + '</td>' +
					'<td style="width:330px;">' + role + '</td>' +
					'<td class="nameList" >' + '' + '</td>' +
					'<td>' + status + '</td>' +
					'</tr>';
				$('#tabList').append(str);
				for(var j = 0; j < nodeList[i].nodeActor.length; j++) {	
					var nameArr = [];	
					var name = nodeList[i].nodeActor[j].actorName;
					var name1 = name+',';
					nameArr.push(name1);
					var str1 = String(nameArr);
					var str2 = str1.substr(0, str1.length);
					if(j == nodeList[i].nodeActor.length-1){
						str2 = str1.substr(0 , str1.length-1);
					}
					$('.nameList').eq(i-1).append(str2);
				}
				
			}
			
			
			
			
			
			
			//审批流程渲染
			var content = '';
			if(tabList[0].approveContent == '') {
				content = '提交';
			}

			var listStr1 = '<li>' +
				'<span>' + tabList[0].approvePerson + '</span>' +
				'<span>' + tabList[0].approveTime + '</span>' +
				'<span class="li_item active">' + content + '</span>' +
				'</li>';
			$('.list-content ul').append(listStr1);
			$('list-content ul li .li_item').eq(0).addClass('active');
			var firstName = tabList[0].approvePerson;
			var obj = '';
			for(var i = 1; i < tabList.length; i++) {
				if(tabList[i].approveTime == 'None' || tabList[i].nodeNo == 'approval201Node' || tabList[i].nodeNo == 'approval202Node' || tabList[i].nodeNo == 'approval207Node' || tabList[i].nodeNo == 'approval208Node') {
					continue;
				}
				var listStr = '<li>' +
					'<span>' + tabList[i].approvePerson + '</span>' +
					'<span>' + tabList[i].approveTime + '</span>' +
					'<span class="li_item">' + '' + '</span>' +
					'</li>';
				$('.list-content ul').append(listStr);

				if(tabList[i].approveContent == ''){
					$('.li_item').eq(i).html('审批中');
					$('.li_item').eq(i).addClass('active2');
				}
				
				if(tabList[i].approveContent) {
					var obj1 = tabList[i].approveContent;
					if(obj1.indexOf("流程自动跳过!") > 0){
						$('.li_item').eq(i).html('跳过');
					    $('.li_item').eq(i).addClass('active3');
						continue;
					}
					var obj2 = JSON.parse(obj1);
					obj = obj2;
				}
				

				if(obj.statusName == '同意') {
					$('.li_item').eq(i).html('通过');
					$('.li_item').eq(i).addClass('active');
				}else if(obj.statusName == '有条件通过'){
					$('.li_item').eq(i).html('有条件通过');
					$('.li_item').eq(i).addClass('active');
				}else if(obj.statusName == '否决') {
					$('.li_item').eq(i).html('否决');
					$('.li_item').eq(i).addClass('active1');
				}else if(obj.statusName == '驳回'){
					$('.li_item').eq(i).html('驳回');
					$('.li_item').eq(i).addClass('active1');
					$('.list-content div #useName').html(tabList[0].approvePerson);
					$('.list-content div #useStatus').html('审批中');
				}
				
				
				
			}
		}
