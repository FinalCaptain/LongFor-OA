/**
 * Created by wangshuang on 17/12/23.
 */


//获取token地址
var url_token = "https://uatapi-longcity.longfor.com:27235/longcity/services/v1/index/ssoLogin"; //已经有了
//强制登陆地址
var url_login = 'http://demo-auth0.oa.longhu.net/'; //已经有了
//流程信息接口
var url_interface_flow = 'http://111.207.120.1:9030/flowapi/flow-interface';
//业务信息接口
var url_interface_business = 'https://uatapi-longcity.longfor.com:27235/longcity/services/v1/bpmWorkflow/select'; //已经有了
//龙城地图地址
var url_city_map = "https://uatlongcity.longfor.com:27236/longcity-web/#/heavyDetailEntry/heavyDetail/";
//审批流接口
var url_bpmWorkFlow = 'https://uatapi-longcity.longfor.com:27235/longcity/services/v1/bpmWorkflow/update';
//龙城地图查看项目报告书
var url_project_book_map = "https://uatlongcity.longfor.com:27236/longcity-web/#/overview/";




var account; // = "1c42c773-b7a4-4a71-a3d2-c1dee69e2973";
var token;
var userCode; //用户名
var searchArrNew = GetQueryString(); //解析地址
var flowNo = searchArrNew.InstanceId;
var instanceId = searchArrNew.InstanceId;
var toDoId = searchArrNew.WorkItemID;
var Mode = searchArrNew.Mode;
var workItemType = searchArrNew.WorkItemType;
var nodeNo;
//  submit(toDoId, instanceId, workItemId, isNormal, operateType, userCode)

$(function () {
  account = $.cookie("account"); //正式时解开注释
  $.ajax({
    type: "post",
    async: false,
    headers: {
      'version': "v1",
      'platform': "ios",
      "Content-Type": "application/json;charset=UTF-8"
    },
    data: JSON.stringify({
      "account": account
    }),
    dataType: "json",
    url: url_token,
    success: function (data) {
      if (data.code == 200) {
        token = data.result.token;
        userCode = data.result.loginName;
        businessInterface(token, flowNo, userCode);
      } else {
        // alert("未登录跳转OA登陆页面")
        window.location = url_login; //正式时解开注释
      }
    }
  })
})
//判断是否为空
function getEmptyStr(data) {
  if (data == null || data == undefined || data === "undefined") {
    return '';
  } else {
    return data;
  }
}

function alert(e, src,handle) {
  $("body").append('<div class="mark">' +
    '<div class="mark-information">' +
    '<div class="mark-message"><img src="' + src + '" alt="">' + e + '</div>' +
    '<div class="close">确定</div>' +
    '</div>' +
    '</div>');
  $(".close").click(function () {
    $(".mark").remove();
	if(handle){
		handle();
	}
  });
}
// businessInterface(token);
function businessInterface(token, flowNo, userCode) {
  if (userCode == null || userCode == undefined || userCode === "undefined" || userCode == "") {
    userCode = 'admin';
  }
  var bznsData;
  $.ajax({
    url: url_interface_business,
    type: 'POST', //GET
    /*data: JSON.stringify({
      flowNo:flowNo,  //正式使用
      // flowNo: "8dc2a3d3-ff2e-4b35-9993-a99995cc5e1e", //测试
      systemNo: "LONGCITY",
      usercode: userCode,
	  businessType:'LCHAP'
    }),*/
    headers: {
      'token': token,
      'version': "v1",
      'platform': "ios",
      "Content-Type": "application/json;charset=UTF-8"
    },
    data: JSON.stringify({
      instanceId: flowNo, //正式使用
      // flowNo: "8dc2a3d3-ff2e-4b35-9993-a99995cc5e1e", //测试
      flag: '2',
      assetType: '1',
      userCode: userCode
    }),
    contentType: 'application/json;charset=UTF-8',
    dataType: 'json', //返回的数据格式：json/xml/html/script/jsonp/text
    beforeSend: function (xhr) {
      $('#loading').show();
      $('#pc').hide();
    },
    success: function (data) {
      $('#loading').hide();
      $('#pc').show();
	  if(data.code == 500){
		  alert(data.message, './heavy/images/error.png',function(){
			  window.location=url_login;
		  });
	  }
      if (data && data.result != null && data.result != undefined && data.result != 'undefined') {

        //bznsData = JSON.parse(data.data.bznsData);

        //var result = bznsData.nofunction.result;
        var result = data.result;
        if (getEmptyStr(result)) {
          var str = '<ul>' +
            '<li>' +
            '<h5>公司</h5>' +
            '<p>' + getEmptyStr(result.ownerCompanyName) + '</p>' +
            '</li>' +
            '<li>' +
            '<h5>项目城市</h5>' +
            '<p>' + getEmptyStr(result.cityCode) + '</p>' +
            '</li>' +
            '<li>' +
            '<h5>行政区</h5>' +
            '<p>' + getEmptyStr(result.areaCode) + '</p>' +
            '</li>' +
            '<li>' +
            '<h5>获取方式</h5>' +
            '<p>' + getEmptyStr(result.accessMethods) + '</p>' +
            '</li>' +
            '<li>' +
            '<h5>合作方式</h5>' +
            '<p>' + getEmptyStr(result.cooperationWay) + '</p>' +
            '</li>' +
            '<li>' +
            '<h5>业务类型</h5>' +
            '<p>' + getEmptyStr(result.businessType) + '</p>' +
            '</li>' +
            '<li>' +
            '<h5>审批阶段</h5>' +
            '<p>' + getEmptyStr(result.auctionStatus) + '</p>' +
            '</li>' +
            '</ul>';
          var strInfo = '<ul>' +
            '<li>' +
            '<h5>竞买方式</h5>' +
            '<p>' + getEmptyStr(result.auctionType) + '</p>' +
            '</li>' +
            '<li>' +
            '<h5>起始总价</h5>' +
            '<p>' + getEmptyStr(result.startingPrice) + ' 万元</p>' +
            '</li>' +
            '<li>' +
            '<h5>建设用地面积</h5>' +
            '<p>' + getEmptyStr(result.planedBuildingArea) + 'm²</p>' +
            '</li>' +
            '<li>' +
            '<h5>计容地上面积</h5>' +
            '<p>' + getEmptyStr(result.floorCoveredArea) + 'm²</p>' +
            '</li>' +
            '<li>' +
            '<h5>综合容积率</h5>' +
            '<p>' + getEmptyStr(result.plotRatio) + '</p>' +
            '</li>' +
            '<li>' +
            '<h5>规划用地性质</h5>' +
            '<p>' + getEmptyStr(result.planedLandTitle) + '</p>' +
            '</li>' +
            '<li>' +
            '<h5>地上计容住宅面积</h5>' +
            '<p>' + getEmptyStr(result.floorPlanHouseArea) + 'm²</p>' +
            '</li>' +
            '</ul>';

          var strPoint = '<ul>' +
            '<li>' +
            '<h5>建议楼面地价</h5>' +
            '<p>' + getEmptyStr(result.adviceFloorPriceRatio) + '元/平米</p >' +
            '</li>' +
            '<li>' +
            '<h5>项目总体IRR</h5>' +
            '<p>' + getEmptyStr(result.proTotalIRR) + '</p >' +
            '</li>' +
            '<li>' +
            '<h5>销售物业净利润</h5>' +
            '<p>' + getEmptyStr(result.tenementProfits) + '万元</p >' +
            '</li>' +
            '<li>' +
            '<h5>销售物业毛利率</h5>' +
            '<p>' + getEmptyStr(result.tenementGrossMargin) + '%</p >' +
            '</li>' +
            '<li>' +
            '<h5>销售物业净利率</h5>' +
            '<p>' + getEmptyStr(result.tenementProfitsRate) + '%</p >' +
            '</li>' +
            '<li>' +
            '<h5>销售物业IRR</h5>' +
            '<p>' + getEmptyStr(result.saleTenementIRR) + '%</p >' +
            '</li>' +
            '<li>' +
            '<h5>留存物业IRR</h5>' +
            '<p>' + getEmptyStr(result.remainTenementIRR) + '%</p >' +
            '</li>' +
            '<li>' +
            '<h5>运营第三年商场租金及物业Y-COST(NPI/总成本)</h5>' +
            '<p>' + getEmptyStr(result.thirdYearMarketRent) + '%</p >' +
            '</li>' +
            '</ul>';

          $('#intro').append(str);
          $('#introInfo').append(strInfo);
          $('#introPoint').append(strPoint);
          $('#headerName').text(result.name);
        }
      }

      //项目落位及详情跳转链接      
      if (getEmptyStr(result)) {
        $('.Heavy-Info').click(function () {

          var address = url_city_map;
          window.location.href = address + result.projectId + "/" + encodeURIComponent(result.name) + "/" + result.longitude + "/" + result.latitude + "/" + token;
        });
      }
      //查看项目报告书跳转链接
      if (getEmptyStr(result)) {
        $('.Heavy-report').click(function () {

          var address = url_project_book_map;
          window.location.href = address + result.projectId + "/" + result.cityCode + "/"+ token;
        });
      }

    },
    error: function (error) {
      console.log(error);
    },
    complete: function () {
      $.ajax({
        url: url_interface_business,
        type: 'POST', //GET
        /*data: JSON.stringify({
          flowNo:flowNo,  //正式使用
          // flowNo: "8dc2a3d3-ff2e-4b35-9993-a99995cc5e1e", //测试
          systemNo: "LONGCITY",
          usercode: userCode,
          businessType:'LCHAP'
        }),*/
        headers: {
          'token': token,
          'version': "v1",
          'platform': "ios",
          "Content-Type": "application/json;charset=UTF-8"
        },
        data: JSON.stringify({
          instanceId: flowNo, //正式使用
          // flowNo: "8dc2a3d3-ff2e-4b35-9993-a99995cc5e1e", //测试
          flag: '1',
          assetType: '1',
          userCode: userCode
        }),
        contentType: 'application/json;charset=UTF-8',
        dataType: 'json', //返回的数据格式：json/xml/html/script/jsonp/text
        beforeSend: function (xhr) {
          $('#loading').show();
          $('#pc').hide();
        },
        success: function (data) {
          var bznsData = data.result.functional;
          flowInterface(token, bznsData, userCode);
        }
      });

    }
  });
}

function flowInterface(token, bznsData, userCode) {
  $.ajax({
    url: url_interface_flow,
    type: 'POST', //GET
    data: JSON.stringify({
      flowNo: flowNo, //正式使用

      // flowNo: "b86b40a6-3148-4c5f-a8e5-2c1c27109a78",//成本
      // flowNo: "8dc2a3d3-ff2e-4b35-9993-a99995cc5e1e",
      systemNo: "LONGCITY"
    }),
    contentType: 'application/json;charset=UTF-8',
    dataType: 'json', //返回的数据格式：json/xml/html/script/jsonp/text
    beforeSend: function (xhr) {
      $('#loading').show();
      $('#pc').hide();
    },
    success: function (data) {
      $('#loading').hide();
      $('#pc').show();
      flowInterfaceWs(bznsData, data);
    },
    error: function (error) {
      console.log(error);
    },
    complete: function () {

      //地总审批意见
      $('#approval200NodeAgreement').click(function () {
        // submit('9b9df522-f20e-42d1-9e89-9e5d4186cbf0', '8dc2a3d3-ff2e-4b35-9993-a99995cc5e1e', 'approval200Node', 1, 1, 'songdi');

        if ($('.texts').val() == '' || $.trim($('.texts').val()) == '') {
          alert('请在评审意见框内发表评审意见', './heavy/images/error.png');

        } else {
          submit(toDoId, instanceId, nodeNo, 1, 1, userCode);

        }
      });
      $('#approval200NodeAgainst').click(function () {
        // submit('9b9df522-f20e-42d1-9e89-9e5d4186cbf0', '8dc2a3d3-ff2e-4b35-9993-a99995cc5e1e', 'approval200Node', 1, 3, 'songdi');

        if ($('.texts').val() == '' || $.trim($('.texts').val()) == '') {
          alert('请在评审意见框内发表评审意见', './heavy/images/error.png');
        } else {
          submit(toDoId, instanceId, nodeNo, 1, 3, userCode);
        }
      });
      //投发审批意见按钮
      $('#approval201NodeAgreement').click(function () {
        // submit('9b9df522-f20e-42d1-9e89-9e5d4186cbf0', '8dc2a3d3-ff2e-4b35-9993-a99995cc5e1e', 'approval201Node', 1, 1, 'songdi');

        if ($('.texts').val() == '' || $.trim($('.texts').val()) == '') {
          alert('请在评审意见框内发表评审意见', './heavy/images/error.png');
        } else {
          submit(toDoId, instanceId, nodeNo, 1, 1, userCode);

        }
      });
      $('#approval201NodeAgainst').click(function () {
        // submit('9b9df522-f20e-42d1-9e89-9e5d4186cbf0', '8dc2a3d3-ff2e-4b35-9993-a99995cc5e1e', 'approval201Node', 1, 3, 'songdi');
        //  submit(toDoId, instanceId, workItemId, isNormal, operateType, userCode)

        if ($('.texts').val() == '' || $.trim($('.texts').val()) == '') {
          alert('请在评审意见框内发表评审意见', './heavy/images/error.png');
        } else {
          submit(toDoId, instanceId, nodeNo, 1, 3, userCode);

        }

      });
      //评审意见按钮
      $('#approval205NodeAgreement').click(function () {
        // submit('024350c4-7cfb-4864-ba8c-cdecf06ac100', '', 'approval205Node', 1, 1, 'songdi');

        if ($('.texts').val() == '' || $.trim($('.texts').val()) == '') {
          alert('请在评审意见框内发表评审意见', './heavy/images/error.png');
        } else {
          submit(toDoId, instanceId, nodeNo, 1, 1, userCode);

        }
      });
      $('#approval205NodeAgainst').click(function () {
        // submit('024350c4-7cfb-4864-ba8c-cdecf06ac100', '', 'approval205Node', 1, 4, 'songdi');

        if ($('.texts').val() == '' || $.trim($('.texts').val()) == '') {
          alert('请在评审意见框内发表评审意见', './heavy/images/error.png');
        } else {
          submit(toDoId, instanceId, nodeNo, 1, 4, userCode);

        }
      });
      $('#approval205NodeAgreementOr').click(function () {
        // submit('024350c4-7cfb-4864-ba8c-cdecf06ac100', '', 'approval205Node', 1, 2, 'songdi');
        if ($('.texts').val() == '' || $.trim($('.texts').val()) == '') {
          alert('请在评审意见框内发表评审意见', './heavy/images/error.png');
        } else {
          submit(toDoId, instanceId, nodeNo, 1, 2, userCode);
        }
      });
      //确认上会
      $('#approval204NodeSubmit').click(function () {
        // submit('024350c4-7cfb-4864-ba8c-cdecf06ac100', '', 'approval204Node', 1, 1, 'songdi');

        if ($('.texts').val() == '' || $.trim($('.texts').val()) == '') {
          alert('请在评审意见框内发表评审意见', './heavy/images/error.png');
        } else {
          submit(toDoId, instanceId, nodeNo, 1, 1, userCode);

        }
      });
      //专业
      $('#submit').click(function () {

        //var approval202NodeOrapproval203Node = 'approval202Node' || 'approval203Node';
        // submit('024350c4-7cfb-4864-ba8c-cdecf06ac100', '', approval202NodeOrapproval203Node, 1, 1, 'songdi');
        //submit(toDoId, instanceId, approval202NodeOrapproval203Node, 0, 1, userCode)
        /*if(!$('.texts').html()){
            alert('请在各个评审意见框内发表评审意见');
        }else{
          //var approval202NodeOrapproval203Node = 'approval202Node' || 'approval203Node';
          // submit('024350c4-7cfb-4864-ba8c-cdecf06ac100', '', approval202NodeOrapproval203Node, 1, 1, 'songdi');
          submit(toDoId, instanceId, nodeNo, 0, 1, userCode);
        }*/

        var obj = $('.texts');
        var status = true;
        for (var i = 0; i < obj.length; i++) {

          if ($.trim($(obj[i]).val()) == '') {
            status = false;
          }
        }
        if (status) {
          submit(toDoId, instanceId, nodeNo, 0, 1, userCode);
        } else {
          alert('请在各个评审意见框内发表评审意见', './heavy/images/error.png');
        }

      });
    }
  });
}

function flowInterfaceWs(bznsData, data) {
  var jsonDataTitle = '';
  if (bznsData) {
    jsonDataTitle = bznsData;
  }

  var flowData = JSON.parse(data.data.flowData);

  var approve_record = flowData.approve_record.approve_record;
  var flowStatus = flowData.flow_data;
  var nodeList = flowData.flow_data.nodeList;
  var tabList = flowData.approve_record.approve_record;
  approvalWeb(nodeList, tabList, flowStatus);

  //
  var nodeListStatus2 = [];
  for (var i = 0; i < nodeList.length; i++) {
    if (nodeList[i].nodeStatus === '2') {
      nodeListStatus2.push(nodeList[i]);
    }
  }

  if (nodeListStatus2.length > 0) {
    nodeNo = nodeListStatus2[0].nodeNo;
  }

  if (!nodeListStatus2 || nodeListStatus2.length == 0) {
    nodeListStatus2[0] = 'x';
    nodeListStatus2[0].nodeNo = "";
  }
  //
  var dz = '';
  if (workItemType == 'Approve'&&Mode == 'Work') {
  
      if (nodeListStatus2[0].nodeNo == 'approval200Node') {

        dz = '<div class="local-opinion">' +
          '<div class="opinion-title">' +
          '<strong></strong>' +
          '<span>地总审批意见</span>' +
          '</div>' +
          '<div class="opinion-information">' +
          '<div class="information">' +
          '<textarea class="texts" placeholder="请填写意见" value=""  style="resize:none;display:block;border: none;" maxlength="1500"></textarea>' +
          '<div class="count">' +
          '<span class="number">0</span>' +
          '<span>/1500</span>' +
          '</div>' +
          '</div>' +
          '<div class="options">' +
          '<button class="agree active" id="approval200NodeAgreement">同意</button>' +
          '<button class="rejected" id="approval200NodeAgainst">驳回</button>' +
          '</div>' +
          '</div>' +
          '</div>';
      } else if (nodeListStatus2[0].nodeNo == 'approval201Node') {
        dz = '<div class="local-opinion">' +
          '<div class="opinion-title">' +
          '<strong></strong>' +
          '<span>投发审批意见</span>' +
          '</div>' +
          '<div class="opinion-information">' +
          '<div class="information">' +
          '<textarea class="texts" placeholder="请填写意见" value=""  style="resize:none;display:block;border: none;" maxlength="1500"></textarea>' +
          '<div class="count">' +
          '<span class="number">0</span>' +
          '<span>/1500</span>' +
          '</div>' +
          '</div>' +
          '<div class="options">' +
          '<button class="agree active" id="approval201NodeAgreement">同意</button>' +
          '<button class="rejected" id="approval201NodeAgainst">驳回</button>' +
          '</div>' +
          '</div>' +
          '</div>';
      } else if (nodeListStatus2[0].nodeNo == 'approval202Node' || nodeListStatus2[0].nodeNo == 'approval203Node') {
        var jsonObj = {};
        var jsonData = {
			title: null,
			tfTitle: {
				t1: '市场状况',
				t2: '项目指标及产品',
				t3: '财务指标及商务合作条件',
				t4: '综述及其他'
			},
			kyTitle: {
				t1: '土地及市场环境',
				t2: '产品与价格定位',
				t3: '销量定位',
				t4: '综合评价/风险提示'
			},
			cbTitle: {
				t1: '项目指标及业态组合',
				t2: '配置标准及设计原型',
				t3: '特殊事项',
				t4: '测算标准及逻辑'
			},
			cwTitle: {
				t1: '综合投资指标评价',
				t2: '测算假设合理性',
				t3: '结利要求',
				t4: '其他'
			},
			yfTitle: {
				t1: '业态配比和极差关系',
				t2: '项目定位和目标客户',
				t3: '可售比和配置标准',
				t4: '规划报批，出让条件解读及其他风险'
			},
			yyTitle: {
				t1: '基本意见1',
				t2: '基本意见2',
				t3: '基本意见3',
				t4: '基本意见4'
			},
			syTitle: {
				t1: '区位、人口及交通',
				t2: '竞品分析',
				t3: '规划方案设计',
				t4: '投资收益分析'
			},
			gyTitle: {
				t1: '市场客户及产品',
				t2: '工程节点及成本指标',
				t3: '财务指标及商务合作条件',
				t4: '综述及其他'
			},
			jdTitle: {
				t1: '市场概况',
				t2: '酒店KPI',
				t3: '品牌建议',
				t4: '综述及其他'
			},
			xzlTitle: {
				t1: '综述',
				t2: '市场状况',
				t3: '定位落地性',
				t4: '运营支出及其他'
			},
			kTitle: {
				t1: '',
				t2: '',
				t3: '',
				t4: ''
			},
			yfsyTitle: {
				t1: '规划设计条件结构化',
				t2: '关键指标不满足跳红',
				t3: '对应风险50问的风险类',
				t4: '产品风险指数评价'
			}
		} 
		//职能区分4个标题
		console.log('====',jsonDataTitle);

		if (jsonDataTitle === '成本') {
			jsonData.title = jsonData.cbTitle;
		} else if (jsonDataTitle === '投发') {
			jsonData.title = jsonData.tfTitle;
		} else if (jsonDataTitle === '客研') {
			jsonData.title = jsonData.kyTitle;
		} else if (jsonDataTitle === '财务') {
			jsonData.title = jsonData.cwTitle;
		} else if (jsonDataTitle === '研发(住宅)') {
			jsonData.title = jsonData.yfTitle;
		} else if (jsonDataTitle === '研发(商业)') {
			jsonData.title = jsonData.yfsyTitle;
		} else if (jsonDataTitle === '运营') {
			jsonData.title = jsonData.yyTitle;
		} else if (jsonDataTitle === '冠寓') {
			jsonData.title = jsonData.gyTitle;
		} else if (jsonDataTitle === '写字楼') {
			jsonData.title = jsonData.xzlTitle;
		} else if (jsonDataTitle === '商业') {
			jsonData.title = jsonData.syTitle;
		} else if (jsonDataTitle === '酒店') {
			jsonData.title = jsonData.jdTitle;
		} else if (!jsonDataTitle) {
          dz = '';
          // return false;
        } else {
			jsonData.title = jsonData.kTitle;
		};

        
		
		
        if (!jsonDataTitle) {
          dz = '';
        } else {
          var pic1 =
            '<div class="fileItem">' +
            '<input type="button" value=""  onclick="document.getElementById(\'PicInput1\').click();" />' +
            '<img id="imgPicInput1" src="./heavy/images/pic2.png">' +
            '</div>' +
            '<div class="fileItem">' +
            '<input type="button" value=""  onclick="document.getElementById(\'PicInput2\').click();" />' +
            '<img id="imgPicInput2" src="./heavy/images/pic2.png">' +
            '</div>' +
            '<div class="fileItem">' +
            '<input type="button" value=""  onclick="document.getElementById(\'PicInput3\').click();" />' +
            '<img id="imgPicInput3" src="./heavy/images/pic2.png">' +
            '</div>' +
            '<div class="fileItem">' +
            '<input type="button" value=""  onclick="document.getElementById(\'PicInput4\').click();" />' +
            '<img id="imgPicInput4" src="./heavy/images/pic2.png">' +
            '</div>';
          var pic2 =
            '<div class="fileItem">' +
            '<input type="button" value=""  onclick="document.getElementById(\'PicInput5\').click();" />' +
            '<img id="imgPicInput5" src="./heavy/images/pic2.png">' +
            '</div>' +
            '<div class="fileItem">' +
            '<input type="button" value=""  onclick="document.getElementById(\'PicInput6\').click();" />' +
            '<img id="imgPicInput6" src="./heavy/images/pic2.png">' +
            '</div>' +
            '<div class="fileItem">' +
            '<input type="button" value=""  onclick="document.getElementById(\'PicInput7\').click();" />' +
            '<img id="imgPicInput7" src="./heavy/images/pic2.png">' +
            '</div>' +
            '<div class="fileItem">' +
            '<input type="button" value=""  onclick="document.getElementById(\'PicInput8\').click();" />' +
            '<img id="imgPicInput8" src="./heavy/images/pic2.png">' +
            '</div>';
          var pic3 =
            '<div class="fileItem">' +
            '<input type="button" value=""  onclick="document.getElementById(\'PicInput9\').click();" />' +
            '<img id="imgPicInput9" src="./heavy/images/pic2.png">' +
            '</div>' +
            '<div class="fileItem">' +
            '<input type="button" value=""  onclick="document.getElementById(\'PicInput10\').click();" />' +
            '<img id="imgPicInput10" src="./heavy/images/pic2.png">' +
            '</div>' +
            '<div class="fileItem">' +
            '<input type="button" value=""  onclick="document.getElementById(\'PicInput11\').click();" />' +
            '<img id="imgPicInput11" src="./heavy/images/pic2.png">' +
            '</div>' +
            '<div class="fileItem">' +
            '<input type="button" value=""  onclick="document.getElementById(\'PicInput12\').click();" />' +
            '<img id="imgPicInput12" src="./heavy/images/pic2.png">' +
            '</div>';
          var pic4 =
            '<div class="fileItem">' +
            '<input type="button" value=""  onclick="document.getElementById(\'PicInput13\').click();" />' +
            '<img id="imgPicInput13" src="./heavy/images/pic2.png">' +
            '</div>' +
            '<div class="fileItem">' +
            '<input type="button" value=""  onclick="document.getElementById(\'PicInput14\').click();" />' +
            '<img id="imgPicInput14" src="./heavy/images/pic2.png">' +
            '</div>' +
            '<div class="fileItem">' +
            '<input type="button" value=""  onclick="document.getElementById(\'PicInput15\').click();" />' +
            '<img id="imgPicInput15" src="./heavy/images/pic2.png">' +
            '</div>' +
            '<div class="fileItem">' +
            '<input type="button" value=""  onclick="document.getElementById(\'PicInput16\').click();" />' +
            '<img id="imgPicInput16" src="./heavy/images/pic2.png">' +
            '</div>';
          dz = '<div class=" professional-opinion">' +
            '<div class="opinion-title">' +
            '<strong></strong>' +
            '<span id="jsonDataTitle" data-id=' + jsonDataTitle + '>本专业评审意见（' + jsonDataTitle + '）</span>' +
            '</div>' +
            '<div class="opinion-information">' +
            '<div class="information-box">' +
            '<div class="information-title">' +
            jsonData.title.t1 +
            '</div>' +
            '<div class="information">' +
            '<textarea class="texts information-texts" placeholder="请填写意见" value=""  style="resize:none;display:block;border: none;" maxlength="1500"></textarea>' +
            '<div class="count">' +
            '<span class="number">0</span>' +
            '<span>/1500</span>' +
            '</div>' +
            '</div>' +
            '<div class="picture-box" id="picture-box1">' +
            pic1 +
            '</div>' +
            '</div>' +
            '<div class="information-box">' +
            '<div class="information-title">' +
            jsonData.title.t2 +
            '</div>' +
            '<div class="information">' +
            '<textarea class="texts information-texts" placeholder="请填写意见" value=""  style="resize:none;display:block;border: none;" maxlength="1500"></textarea>' +
            '<div class="count">' +
            '<span class="number">0</span>' +
            '<span>/1500</span>' +
            '</div>' +
            '</div>' +
            '<div class="picture-box" id="picture-box2">' +
            pic2 +
            '</div>' +
            '</div>' +
            '<div class="information-box">' +
            '<div class="information-title">' +
            jsonData.title.t3 +
            '</div>' +
            '<div class="information">' +
            '<textarea class="texts information-texts" placeholder="请填写意见" value=""  style="resize:none;display:block;border: none;" maxlength="1500"></textarea>' +
            '<div class="count">' +
            '<span class="number">0</span>' +
            '<span>/1500</span>' +
            '</div>' +
            '</div>' +
            '<div class="picture-box" id="picture-box3">' +
            pic3 +
            '</div>' +
            '</div>' +
            '<div class="information-box">' +
            '<div class="information-title">' +
            jsonData.title.t4 +
            '</div>' +
            '<div class="information">' +
            '<textarea class="texts information-texts" placeholder="请填写意见" value=""  style="resize:none;display:block;border: none;" maxlength="1500"></textarea>' +
            '<div class="count">' +
            '<span class="number">0</span>' +
            '<span>/1500</span>' +
            '</div>' +
            '</div>' +
            '<div class="picture-box" id="picture-box4">' +
            pic4 +
            '</div>' +
            '</div>' +
            '<div class="options">' +
            '<button class="agree active" id="submit">提交</button>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<input id="filess" style="display: none">';
        }
      } else if (nodeListStatus2[0].nodeNo == 'approval204Node') {
        dz = '<div class="local-opinion">' +
          '<div class="opinion-title">' +
          '<strong></strong>' +
          '<span>集团投发领导确认上会</span>' +
          '</div>' +
          '<div class="opinion-information">' +
          '<div class="information">' +
          '<textarea class="texts" placeholder="请填写意见" value=""  style="resize:none;display:block;border: none;" maxlength="1500"></textarea>' +
          '<div class="count">' +
          '<span class="number">0</span>' +
          '<span>/1500</span>' +
          '</div>' +
          '</div>' +
          '<div class="options">' +
          '<button class="agree active" id="approval204NodeSubmit">提交</button>' +
          '</div>' +
          '</div>' +
          '</div>';
      } else if (nodeListStatus2[0].nodeNo == 'approval205Node') {
        dz = '<div class="local-opinion">' +
          '<div class="opinion-title">' +
          '<strong></strong>' +
          '<span>审批意见</span>' +
          '</div>' +
          '<div class="opinion-information">' +
          '<div class="information">' +
          '<textarea class="texts" placeholder="请填写意见" value=""  style="resize:none;display:block;border: none;" maxlength="1500"></textarea>' +
          '<div class="count">' +
          '<span class="number">0</span>' +
          '<span>/1500</span>' +
          '</div>' +
          '</div>' +
          '<div class="options">' +
          '<button class="agree active" id="approval205NodeAgreement">同意</button>' +
          '<button class="conditions" id="approval205NodeAgreementOr">有条件通过</button>' +
          '<button class="rejected" id="approval205NodeAgainst">否决</button>' +
          '</div>' +
          '</div>' +
          '</div>';


      } else {
        dz = '';
      }
   

  } else{
    dz = '';
  }
  if (dz == '') {
    $('.opinions').css('margin-bottom', 0);
    $('.record-body').css('height', '100%');
    $('#scrollRecord').css('overflow-y', 'visible');
  }
  $('.opinions').append(dz);


  var node = $(".texts");
  var cpLock = false;
  node.on('compositionstart', function () {
    cpLock = true;
  });
  node.on('compositionend', function () {
    cpLock = false;
    calNum(this);
  });
  node.on('input', function () {
    calNum(this);
  });
  //字符串截断函数
  function calNum(domEle) {
    if (!cpLock) {
      var maxLen = 1500;
      var curtLen = domEle.value.length;
      if (curtLen > maxLen) {
        domEle.value = domEle.value.substring(0, maxLen);
        alert("输入内容不能超过1500个字符", './heavy/images/error.png');
      }
      $(".number").text(domEle.value.length.toString());
    }
  }

  var recordHtml = '';
  for (var j = 1; j < approve_record.length; j++) {

    var content = approve_record[j].approveContent;
    var approveContent;
    if (!content) {
      continue;
    } else {
      if (content.indexOf("流程自动跳过!") > 0) {
        continue;
      }
      approveContent = JSON.parse(content);
    }

    if (approveContent) {
      if (approveContent.isNormal === '1') {
        if (approveContent.opinion) {
          recordHtml += '<li class="work">' +
            '<p class="label">' + approve_record[j].approvePerson + '</p>' +
            '<span class="date">' + approve_record[j].approveTime + '</span>' +
            '<span class="circle"></span>' +
            '<div class="content-time">' +
            '<p class="text-time">' + approveContent.opinion + '</p>' +
            '</div>' +
            '</li>';
        }

      } else {
        var str1 = '';
        for (var x = 1; x < 5; x++) {
          var obj = 'obj' + x;
          str1 += '<div>' +
            '<h3 class="content-time-title">' + approveContent[obj].keyName + '</h3>' +
            '<div class="content-time-content text-time">' + approveContent[obj].value + '</div>' +
            '<div>';
          if (approveContent[obj].imgs && approveContent[obj].imgs.length > 0) {
            for (var i = 0; i < approveContent[obj].imgs.length; i++) {
              if (!approveContent[obj].imgs[i] || approveContent[obj].imgs[i] === undefined ||
                approveContent[obj].imgs[i] == null || approveContent[obj].imgs[i] === 'undefined') {
                continue;
              }
              str1 += '<img src="' + approveContent[obj].imgs[i] + '" style="width:60px; height: 60px; margin-right: 10px;"/>';
            }

          }
          str1 += '</div>' +
            '</div>';
        }

        recordHtml += '<li class="work">' +
          '<p class="label">' + approve_record[j].approvePerson + '</p>' +
          '<span class="date">' + approve_record[j].approveTime + '</span>' +
          '<span class="circle"></span>' +
          '<div class="content-time">' +
          '<div class="content-time-box">' + str1 + '</div>' +
          '</div>' +
          '</li>'

      }
    }

  }
  if (recordHtml == '') {
    recordHtml = '<li style="color: #ccc;">暂无数据</li>';
    $('#timeline').css('border-left', 'none');
  }
  $('#timeline').append(recordHtml);


}











//解析url 查询参数
function GetQueryString() {
  var url = window.location.search;
  if (url.indexOf("?") != -1) {
    var str = url.substr(1);
    strs = str.split("&");
    var searchArr = {};
    var key = new Array(strs.length);
    var value = new Array(strs.length);
    for (i = 0; i < strs.length; i++) {
      key[i] = strs[i].split("=")[0]
      value[i] = decodeURIComponent(strs[i].split("=")[1]);
      // alert(key[i]+"="+value[i]);
      searchArr[key[i]] = value[i];
    }
  }
  return searchArr;

}


function submit(toDoId, instanceId, workItemId, isNormal, operateType, userCode) {
  $('.options button').attr('disabled', true);
  var statusName = '';
  if (operateType == 1) {
    statusName = '同意';
  } else if (operateType == 2) {
    statusName = '有条件通过';
  } else if (operateType == 3) {
    statusName = '驳回';
  } else if (operateType == 4) {
    statusName = '否决';
  }
  var data = {};
  var dataNew = {};

  if (isNormal == 1) {

    obj = $('.information .texts').val();
    data = {
      toDoId: toDoId,
      instanceId: instanceId,
      workItemId: workItemId,
      commentText: {
        isNormal: isNormal,
        statusName: statusName,
        obj1: {
          keyName: "市场状况",
          value: "",
          imgs: []
        },
        obj2: {
          keyName: "项目指标及产品",
          value: "",
          imgs: []
        },
        obj3: {
          keyName: "财务指标及商务合作条件",
          value: "",
          imgs: []
        },
        obj4: {
          keyName: "综述及其他",
          value: "",
          imgs: []
        },
        functional: '',
        opinion: obj
      },
      operateType: operateType,
      userCode: userCode
    };
    /*
    dataNew = {
      todoId: toDoId,
      systemNo:'LONGCITY',
      data:JSON.stringify(data),
    };
	*/
  } else if (isNormal == 0) {

    function arrNew(arr) {
      var arr1 = [];
      for (var y = 0; y < arr.length; y++) {
        if (arr[y] == null || arr[y] == undefined ||
          arr[y] === 'undefined' || arr[y] == '') {
          continue;
        }
        arr1.push(arr[y]);
      }
      return arr1;
    }



    obj = {
      obj1: {
        keyName: $('.information-title').eq(0).text(),
        value: $('.information-texts').eq(0).val(),
        imgs: arrNew(imgList1)
      },
      obj2: {
        keyName: $('.information-title').eq(1).text(),
        value: $('.information-texts').eq(1).val(),
        imgs: arrNew(imgList2)
      },
      obj3: {
        keyName: $('.information-title').eq(2).text(),
        value: $('.information-texts').eq(2).val(),
        imgs: arrNew(imgList3)
      },
      obj4: {
        keyName: $('.information-title').eq(3).text(),
        value: $('.information-texts').eq(3).val(),
        imgs: arrNew(imgList4)
      }
    };

    data = {
      toDoId: toDoId,
      instanceId: instanceId,
      workItemId: workItemId,
      commentText: {
        isNormal: isNormal,
        statusName: statusName,
        obj1: {
          keyName: obj.obj1.keyName,
          value: obj.obj1.value,
          imgs: obj.obj1.imgs
        },
        obj2: {
          keyName: obj.obj2.keyName,
          value: obj.obj2.value,
          imgs: obj.obj2.imgs
        },
        obj3: {
          keyName: obj.obj3.keyName,
          value: obj.obj3.value,
          imgs: obj.obj3.imgs
        },
        obj4: {
          keyName: obj.obj4.keyName,
          value: obj.obj4.value,
          imgs: obj.obj4.imgs
        },
        functional: $('#jsonDataTitle').data('id'),
        opinion: ''
      },
      operateType: operateType,
      userCode: userCode
    };
    /*
    dataNew = {
      todoId: toDoId,
      systemNo:'LONGCITY',
      data:JSON.stringify(data),
    };
	*/
  };

  $.ajax({
    url: url_bpmWorkFlow,
    type: 'POST', //GET
    headers: {
      'token': token,
      'version': "v1",
      'platform': "ios",
      "Content-Type": "application/json;charset=UTF-8"
    },
    data: JSON.stringify(data),
    //contentType: 'application/json;charset=UTF-8',
    dataType: 'json', //返回的数据格式：json/xml/html/script/jsonp/text
    beforeSend: function (xhr) {
      $('.options button').attr('disabled', true);
    },
    success: function (data) {
		if(data.code == 200){
			var str = '<div class="mark">' +
			'<div class="mark-information">' +
			'<div class="mark-message"><img src="./heavy/images/right.png" alt="">恭喜您，您的意见已成功提交！</div>' +
			'<div class="close">关闭</div>' +
			'</div>' +
			'</div>';
			$('body').append(str);

			$(this).css({
				'background': '#ccc',
				'color': '#999',
				'border-color': '#ccc'
			});
			$('body').on('click', '.mark', function () {
				$(this).remove();
				window.location.href = window.location.href.replace('Work', 'View');
			}).on('click', '.close', function () {
				$('.mark').remove();
				window.location.href = window.location.href.replace('Work', 'View');
			});
		}else if(data.code == 500 ){
			console.log(data.message);
			alert(data.message,"./heavy/images/error.png",function(){
				window.location = url_login+'apps/pages/tasks/';
			});
		}
      
 
    },
    error: function (error) {
      var str = '<div class="mark">' +
        '<div class="mark-information">' +
        '<div class="mark-message"><img src="./heavy/images/error.png" alt="">很抱歉，您的意见未能成功提交，请核对后再次进行操作。</div>' +
        '<div class="close">关闭</div>' +
        '</div>' +
        '</div>';
      $('body').append(str);
      $('.options button').attr('disabled', false);
      $('body').on('click', '.mark', function () {
        $(this).remove();

      }).on('click', '.close', function () {
        $('.mark').remove();
      });
    }
  });


}