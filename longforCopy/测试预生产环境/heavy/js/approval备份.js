			
		function approvalWeb(nodeList,tabList,flowStatus){
			//完整流程表格渲染
			var status = '';
			var flowName = '';
			var role = '';
			var status1 = '';
			if(flowStatus.flowStatus == 0){
		      	$('.tab-title a').html('未完成');
		    }else{
		      	$('.tab-title a').html('已完成');
		    }
			
			if(nodeList[0].nodeStatus == 1) {
				status1 = '未审批';
			} else if(nodeList[0].nodeStatus == 2) {
				status1 = '审批中';
			} else if(nodeList[0].nodeStatus == 3) {
				status1 = '已审批';
			}
			role1 = nodeList[0].nodeName.substr(2);
			var str1 = '<tr class="table_tr">' +
				'<td style="width:100px;">' + 1 + '</td>' +
				'<td style="width:150px;">' + nodeList[0].nodeName + '</td>' +
				'<td style="width:330px;">' + role1 + '</td>' +
				'<td>' + nodeList[0].nodeActor[0].actorName + '</td>' +
				'<td>' + status1 + '</td>' +
				'</tr>'
			$('#tabList').append(str1);
			for(var i = 1; i < nodeList.length; i++) {
				if(nodeList[i].nodeStatus == 1) {
					status = '未审批';
				} else if(nodeList[i].nodeStatus == 2) {
					status = '审批中';
				} else if(nodeList[i].nodeStatus == 3) {
					status = '已审批';
				}
				var strNode = nodeList[i].nodeName
				var flowNum = strNode.lastIndexOf('(');
				var roleNum = strNode.lastIndexOf(')');
				flowName = strNode.substr(0, flowNum);
				role = strNode.substr(flowNum, roleNum);
				//console.log(role);
				//role.replaceAll(", ,",",");
				var str = '<tr class="table_tr">' +
					'<td style="width:100px;">' + (i + 1) + '</td>' +
					'<td style="width:150px;">' + flowName + '</td>' +
					'<td style="width:330px;">' + role + '</td>' +
					'<td class="nameList" >' + '' + '</td>' +
					'<td>' + status + '</td>' +
					'</tr>'
				$('#tabList').append(str);
			}
			
			var nameArr1 = [],
				nameArr2 = [],
				nameArr3 = [],
				nameArr4 = [],
				nameArr5 = [],
				nameArr6 = [],
				nameArr7 = [],
				nameArr8 = [];
			for(var j = 0; j < nodeList[1].nodeActor.length; j++) {
				var name1 = nodeList[1].nodeActor[j].actorName;
				nameArr1.push(name1);
				let str = String(nameArr1);
				var  str1  =  str.substring(0, str.length);
				$('.nameList').eq(0).html(str1);
			}
			for(var j = 0; j < nodeList[2].nodeActor.length; j++) {
				var name2 = nodeList[2].nodeActor[j].actorName;
				nameArr2.push(name2);
				let str = String(nameArr2);
				var  str2  =  str.substring(0, str.length);
				$('.nameList').eq(1).html(str2);
			}
			for(var j = 0; j < nodeList[3].nodeActor.length; j++) {
				var name3 = nodeList[3].nodeActor[j].actorName;
				nameArr3.push(name3);
				let str = String(nameArr3);
				var  str3  =  str.substring(0, str.length);
				$('.nameList').eq(2).html(str3);
			}
			for(var j = 0; j < nodeList[4].nodeActor.length; j++) {
				var name4 = nodeList[4].nodeActor[j].actorName;
				nameArr4.push(name4);
				let str = String(nameArr4);
				var  str4  =  str.substring(0, str.length);
				$('.nameList').eq(3).html(str4);
			}
			for(var j = 0; j < nodeList[5].nodeActor.length; j++) {
				var name5 = nodeList[5].nodeActor[j].actorName;
				nameArr5.push(name5);
				let str = String(nameArr5);
				var  str5  =  str.substring(0, str.length);
				$('.nameList').eq(4).html(str5);
			}
			for(var j = 0; j < nodeList[6].nodeActor.length; j++) {
				var name6 = nodeList[6].nodeActor[j].actorName;
				nameArr6.push(name6);
				let str = String(nameArr6);
				var  str6 =  str.substring(0, str.length);
				$('.nameList').eq(5).html(str6);
			}
			for(var j = 0; j < nodeList[7].nodeActor.length; j++) {
				var name7 = nodeList[7].nodeActor[j].actorName;
				nameArr7.push(name7);
				let str = String(nameArr7);
				var  str7  =  str.substring(0, str.length);
				$('.nameList').eq(6).html(str7);
			}
			for(var j = 0; j < nodeList[8].nodeActor.length; j++) {
				var name8 = nodeList[8].nodeActor[j].actorName;
				nameArr8.push(name8);
				let str = String(nameArr8);
				var  str8  =  str.substring(0, str.length);
				$('.nameList').eq(7).html(str8);
			}
			//顶部流程渲染
			var listItem = $('.ExamineInfo ul li.item-list');
			var listItemP = $('.ExamineInfo ul li.item-list p');

			if(nodeList[0].nodeStatus == 3) {
				listItem.eq(0).addClass('active');
				listItemP.eq(0).html(nodeList[0].nodeName)
			} else if(nodeList[0].nodeStatus == 1) {
				listItem.eq(0).addClass('active1')
				listItemP.eq(0).html(nodeList[0].nodeName)
			} else if(nodeList[0].nodeStatus == 2) {
				listItem.eq(0).addClass('active2')
				listItemP.eq(0).html(nodeList[0].nodeName)
			}

			if(nodeList[1].nodeStatus == 3) {
				listItem.eq(1).addClass('active');
				var flowNum = nodeList[1].nodeName.lastIndexOf('(');
				var roleNum = nodeList[1].nodeName.lastIndexOf(')');
				flowName = nodeList[1].nodeName.substr(0, flowNum);
				listItemP.eq(1).html(flowName);
			} else if(nodeList[1].nodeStatus == 1) {
				listItem.eq(1).addClass('active1');
				var flowNum = nodeList[1].nodeName.lastIndexOf('(');
				var roleNum = nodeList[1].nodeName.lastIndexOf(')');
				flowName = nodeList[1].nodeName.substr(0, flowNum);
				listItemP.eq(1).html(flowName);
			} else if(nodeList[1].nodeStatus == 2) {
				listItem.eq(1).addClass('active2');
				var flowNum = nodeList[1].nodeName.lastIndexOf('(');
				var roleNum = nodeList[1].nodeName.lastIndexOf(')');
				flowName = nodeList[1].nodeName.substr(0, flowNum);
				listItemP.eq(1).html(flowName);
			}

			if(nodeList[2].nodeStatus == 3) {
				listItem.eq(2).addClass('active');
				var flowNum = nodeList[2].nodeName.lastIndexOf('(');
				var roleNum = nodeList[2].nodeName.lastIndexOf(')');
				flowName = nodeList[2].nodeName.substr(0, flowNum);
				listItemP.eq(2).html(flowName);
			} else if(nodeList[2].nodeStatus == 1) {
				listItem.eq(2).addClass('active1');
				var flowNum = nodeList[2].nodeName.lastIndexOf('(');
				var roleNum = nodeList[2].nodeName.lastIndexOf(')');
				flowName = nodeList[2].nodeName.substr(0, flowNum);
				listItemP.eq(2).html(flowName);
			} else if(nodeList[2].nodeStatus == 2) {
				listItem.eq(2).addClass('active2');
				var flowNum = nodeList[2].nodeName.lastIndexOf('(');
				var roleNum = nodeList[2].nodeName.lastIndexOf(')');
				flowName = nodeList[2].nodeName.substr(0, flowNum);
				listItemP.eq(2).html(flowName);
			}

			if(nodeList[3].nodeStatus == 3) {
				listItem.eq(3).addClass('active');
				var flowNum = nodeList[3].nodeName.lastIndexOf('(');
				var roleNum = nodeList[3].nodeName.lastIndexOf(')');
				flowName = nodeList[3].nodeName.substr(0, flowNum);
				listItemP.eq(3).html(flowName);
			} else if(nodeList[3].nodeStatus == 1) {
				listItem.eq(3).addClass('active1');
				var flowNum = nodeList[3].nodeName.lastIndexOf('(');
				var roleNum = nodeList[3].nodeName.lastIndexOf(')');
				flowName = nodeList[3].nodeName.substr(0, flowNum);
				listItemP.eq(3).html(flowName);
			} else if(nodeList[3].nodeStatus == 2) {
				listItem.eq(3).addClass('active2');
				var flowNum = nodeList[3].nodeName.lastIndexOf('(');
				var roleNum = nodeList[3].nodeName.lastIndexOf(')');
				flowName = nodeList[3].nodeName.substr(0, flowNum);
				listItemP.eq(3).html(flowName);
			}

			if(nodeList[4].nodeStatus == 3) {
				listItem.eq(4).addClass('active');
				var flowNum = nodeList[4].nodeName.lastIndexOf('(');
				var roleNum = nodeList[4].nodeName.lastIndexOf(')');
				flowName = nodeList[4].nodeName.substr(0, flowNum);
				listItemP.eq(4).html(flowName);
			} else if(nodeList[4].nodeStatus == 1) {
				listItem.eq(4).addClass('active1');
				var flowNum = nodeList[4].nodeName.lastIndexOf('(');
				var roleNum = nodeList[4].nodeName.lastIndexOf(')');
				flowName = nodeList[4].nodeName.substr(0, flowNum);
				listItemP.eq(4).html(flowName);
			} else if(nodeList[4].nodeStatus == 2) {
				listItem.eq(4).addClass('active2');
				var flowNum = nodeList[4].nodeName.lastIndexOf('(');
				var roleNum = nodeList[4].nodeName.lastIndexOf(')');
				flowName = nodeList[4].nodeName.substr(0, flowNum);
				listItemP.eq(4).html(flowName);
			}

			if(nodeList[5].nodeStatus == 3) {
				listItem.eq(5).addClass('active');
				var flowNum = nodeList[5].nodeName.lastIndexOf('(');
				var roleNum = nodeList[5].nodeName.lastIndexOf(')');
				flowName = nodeList[5].nodeName.substr(0, flowNum);
				listItemP.eq(5).html(flowName);
			} else if(nodeList[5].nodeStatus == 1) {
				listItem.eq(5).addClass('active1');
				var flowNum = nodeList[5].nodeName.lastIndexOf('(');
				var roleNum = nodeList[5].nodeName.lastIndexOf(')');
				flowName = nodeList[5].nodeName.substr(0, flowNum);
				listItemP.eq(5).html(flowName);
			} else if(nodeList[5].nodeStatus == 2) {
				listItem.eq(5).addClass('active2');
				var flowNum = nodeList[5].nodeName.lastIndexOf('(');
				var roleNum = nodeList[5].nodeName.lastIndexOf(')');
				flowName = nodeList[5].nodeName.substr(0, flowNum);
				listItemP.eq(5).html(flowName);
			}

			if(nodeList[6].nodeStatus == 3) {
				listItem.eq(6).addClass('active');
				var flowNum = nodeList[6].nodeName.lastIndexOf('(');
				var roleNum = nodeList[6].nodeName.lastIndexOf(')');
				flowName = nodeList[6].nodeName.substr(0, flowNum);
				listItemP.eq(6).html(flowName);
			} else if(nodeList[6].nodeStatus == 1) {
				listItem.eq(6).addClass('active1');
				var flowNum = nodeList[6].nodeName.lastIndexOf('(');
				var roleNum = nodeList[6].nodeName.lastIndexOf(')');
				flowName = nodeList[6].nodeName.substr(0, flowNum);
				listItemP.eq(6).html(flowName);
			} else if(nodeList[6].nodeStatus == 2) {
				listItem.eq(6).addClass('active2');
				var flowNum = nodeList[6].nodeName.lastIndexOf('(');
				var roleNum = nodeList[6].nodeName.lastIndexOf(')');
				flowName = nodeList[6].nodeName.substr(0, flowNum);
				listItemP.eq(6).html(flowName);
			}

			if(nodeList[7].nodeStatus == 3) {
				listItem.eq(7).addClass('active');
				var flowNum = nodeList[7].nodeName.lastIndexOf('(');
				var roleNum = nodeList[7].nodeName.lastIndexOf(')');
				flowName = nodeList[7].nodeName.substr(0, flowNum);
				listItemP.eq(7).html(flowName);
			} else if(nodeList[7].nodeStatus == 1) {
				listItem.eq(7).addClass('active1');
				var flowNum = nodeList[7].nodeName.lastIndexOf('(');
				var roleNum = nodeList[7].nodeName.lastIndexOf(')');
				flowName = nodeList[7].nodeName.substr(0, flowNum);
				listItemP.eq(7).html(flowName);
			} else if(nodeList[7].nodeStatus == 2) {
				listItem.eq(7).addClass('active2');
				var flowNum = nodeList[7].nodeName.lastIndexOf('(');
				var roleNum = nodeList[7].nodeName.lastIndexOf(')');
				flowName = nodeList[7].nodeName.substr(0, flowNum);
				listItemP.eq(7).html(flowName);
			}

			if(nodeList[8].nodeStatus == 3) {
				listItem.eq(8).addClass('active');
				var flowNum = nodeList[8].nodeName.lastIndexOf('(');
				var roleNum = nodeList[8].nodeName.lastIndexOf(')');
				flowName = nodeList[8].nodeName.substr(0, flowNum);
				listItemP.eq(8).html(flowName);
			} else if(nodeList[8].nodeStatus == 1) {
				listItem.eq(8).addClass('active1');
				var flowNum = nodeList[8].nodeName.lastIndexOf('(');
				var roleNum = nodeList[8].nodeName.lastIndexOf(')');
				flowName = nodeList[8].nodeName.substr(0, flowNum);
				listItemP.eq(8).html(flowName);
			} else if(nodeList[8].nodeStatus == 2) {
				listItem.eq(8).addClass('active2');
				var flowNum = nodeList[8].nodeName.lastIndexOf('(');
				var roleNum = nodeList[8].nodeName.lastIndexOf(')');
				flowName = nodeList[8].nodeName.substr(0, flowNum);
				listItemP.eq(8).html(flowName);
			}
			
			
			//审批流程渲染
			var content = ''
			if(tabList[0].approveContent == '') {
				content = '同意';
			}

			var listStr1 = '<li>' +
				'<span>' + tabList[0].approvePerson + '</span>' +
				'<span>' + tabList[0].approveTime + '</span>' +
				'<span class="li_item active">' + content + '</span>' +
				'</li>'
			$('.list-content ul').append(listStr1);
			$('list-content ul li .li_item').eq(0).addClass('active');
			var obj = '';
			for(var i = 1; i < tabList.length; i++) {
				if(tabList[i].approveTime == 'None') {
					return '';
				}
				var listStr = '<li>' +
					'<span>' + tabList[i].approvePerson + '</span>' +
					'<span>' + tabList[i].approveTime + '</span>' +
					'<span class="li_item">' + '' + '</span>' +
					'</li>'
				$('.list-content ul').append(listStr);
				if(tabList[i].approveContent) {
					var obj1 = tabList[i].approveContent;
					console.log("sunqi:"+obj1);
					if(obj1 == '审批（四）环节具有相同审批人员，流程自动跳过!'){
						continue;
					}
					var obj2 = JSON.parse(obj1)
					obj = obj2;
				}

				if(obj.statusName == '同意') {
					$('.li_item').eq(i).html('通过')
					$('.li_item').eq(i).addClass('active');
				}else if(obj.statusName == '有条件通过'){
					$('.li_item').eq(i).html('有条件通过');
					$('.li_item').eq(i).addClass('active');
				}else if(obj.statusName == '否决' || obj.statusName == '驳回') {
					$('.li_item').eq(i).html('驳回');
					$('.li_item').eq(i).addClass('active1');
					$('.list-content div #useName').html(tabList[0].approvePerson);
					$('.list-content div #useStatus').html('审批中');
				}
				
				
			}
		}
