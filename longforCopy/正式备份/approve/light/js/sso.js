/**
 * Created by wangshuang on 17/12/23.
 */
//获取touken地址
var url_token = "https://api-longcity.longfor.com:6443/longcity/services/v1/index/ssoLogin";//郭立明接口
//强制登陆地址
var url_login = "http://www.oa.longhu.net";//强制登陆
//流程信息接口
var url_interface_flow = "https://moapproval.longfor.com:27251/flowapi/flow-interface";//法金接口
//业务信息接口
var url_interface_business ="https://api-longcity.longfor.com:6443/longcity/services/v1/bpmWorkflow/select"; //郭立明接口
//龙城地图地址            
//var url_city_map = "https://longcity.longfor.com:5443/longcity/#/heavyDetailEntry/heavyDetail/";//刘俊纬接口
//审批流接口
var url_bpmWorkFlow = 'https://api-longcity.longfor.com:6443/longcity/services/v1/bpmWorkflow/update';//郭立明接口


var account; // = "1c42c773-b7a4-4a71-a3d2-c1dee69e2973";
var token;
var userCode; //用户名
var searchArrNew = GetQueryString(); //解析地址
var flowNo = searchArrNew.InstanceId;
var workItemType = searchArrNew.WorkItemType;
var instanceId = searchArrNew.InstanceId;
var toDoId = searchArrNew.WorkItemID;
var Mode = searchArrNew.Mode;
var nodeNo;
//  submit(toDoId, instanceId, workItemId, isNormal, operateType, userCode)

$(function () {
  account = $.cookie("account"); //正式时解开注释
  $.ajax({
    type: "post",
    async: false,
    headers: {
      'version': "v1",
      'platform': "ios",
      "Content-Type": "application/json;charset=UTF-8"
    },
    data: JSON.stringify({
      "account": account
    }),
    dataType: "json",
    url: url_token,
    success: function (data) {
      if (data.code == 200) {
        token = data.result.token;
        userCode = data.result.loginName;
        LightService(token, flowNo, userCode);
      } else {
        // alert("未登录跳转OA登陆页面")
        window.location = url_login; //正式时解开注释
      }
    }
  })
})
//判断是否为空
function getEmptyStr(data) {
  if (data == null || data == undefined || data === "undefined") {
    return '';
  } else {
    return data;
  }
}






//判断是否为空
function LightService(token, flowNo, userCode) {
  if (userCode == null || userCode == undefined || userCode === "undefined" || userCode == "") {
    userCode = 'admin';
  }
  var bznsData;
  $.ajax({
    url: url_interface_business,
    type: 'POST', //GET
    /*data: JSON.stringify({
      flowNo:flowNo,  //正式使用
      // flowNo: "8dc2a3d3-ff2e-4b35-9993-a99995cc5e1e", //测试
      systemNo: "LONGCITY",
      usercode: userCode,
      businessType:'LCHAP'
    }),*/
    headers: {
      'token': token,
      'version': "v1",
      'platform': "ios",
      "Content-Type": "application/json;charset=UTF-8"
    },
    data: JSON.stringify({
      instanceId: flowNo, //正式使用
      // flowNo: "8dc2a3d3-ff2e-4b35-9993-a99995cc5e1e", //测试
      flag: '2',
      assetType: '2',
      userCode: userCode
    }),
    contentType: 'application/json;charset=UTF-8',
    dataType: 'json', //返回的数据格式：json/xml/html/script/jsonp/text
    beforeSend: function (xhr) {
      $('#loading').show();
      $('#pc').hide();
    },
    success: function (data) {
      $('#loading').hide();
      $('#pc').show();
      if (data.code == 200) {
        if (data && data.result != null && data.result != undefined && data.result != 'undefined') {
          //bznsData = JSON.parse(data.data.bznsData);
          //var result = bznsData.nofunction.result;
          var result = data.result;
          if (getEmptyStr(result)) {
            //风险分析
            $('#riskZhai').html(getEmptyStr(result.riskAnalysisLight.riskanalyInfor));
            $('#risk').html(getEmptyStr(result.riskAnalysisLight.riskanalyRiskCategories.join(' , ')));

            //投资指标
            $('#InvestAB').html(getEmptyStr(result.investmentIndex.investmentTargetInfor));
            $('#InvestYeah').html(getEmptyStr(result.investmentIndex.investarDynamicPaybackPeriod + " &nbsp 年")); //动态投资回收期
            $('#InvestCost').html(getEmptyStr(result.investmentIndex.investarThirdYearCost + " &nbsp %")); //运营第三年Y-cost
            $('#InvestIRR').html(getEmptyStr(result.investmentIndex.investarIRR + " &nbsp %")); //IRR（不考虑融资）
            $('#InvestNPV').html(getEmptyStr(result.investmentIndex.investarNPV + " &nbsp 万元")); //投资指标NPV

            //成本分析
            $('#costAnalysis').html(getEmptyStr(result.costAnalysis.constanaInfor));
            $('#TotalCost').html(getEmptyStr(result.costAnalysis.constanaTotalCost + " &nbsp 元")); //总建造成本
            $('#ConstructionCost').html(getEmptyStr(result.costAnalysis.constanaConstructionCost + " &nbsp 元")); //冠寓建造成本
            $('#FirstSpaceCost').html(getEmptyStr(result.costAnalysis.constanaFirstSpaceCost + " &nbsp 元")); //一展空间建造成本
            $('#UnilateralCost').html(getEmptyStr(result.costAnalysis.constanaUnilateralCost + " &nbsp 元")); //单方成本(冠寓)
            $('#SingleRoomCost').html(getEmptyStr(result.costAnalysis.constanaSingleRoomCost + " &nbsp 元")); //冠寓单间成本
            $('#ModificationCost').html(getEmptyStr(result.costAnalysis.constanaModificationCost + " &nbsp 元")); //改造成本(冠寓)
            $('#IndoorHardCost').html(getEmptyStr(result.costAnalysis.constanaIndoorHardCost + " &nbsp 元")); //户内硬装成本(冠寓)
            $('#FurnitureCost').html(getEmptyStr(result.costAnalysis.constanaFurnitureCost + " &nbsp 元")); //户内家具成本(冠寓)
            $('#ApplianceCost').html(getEmptyStr(result.costAnalysis.constanaApplianceCost + " &nbsp 元")); //户内家电成本(冠寓)
            $('#1stPubHardSoftCost').html(getEmptyStr(result.costAnalysis.constanaFirstSquareCost + " &nbsp 元")); //一展空间单方成本
            $('#PubHardSoftCost').html(getEmptyStr(result.costAnalysis.constanaPubHardSoftCost + " &nbsp 元")); //公共活动区软硬装成本 
            $('#FirstSquareCost').html(getEmptyStr(result.costAnalysis.constana1stPubHardSoftCost + " &nbsp 元")); //标准层公共区域（含一层过道电梯厅）软硬装成本

            //产品定位
            $('#productPositInfor').html(getEmptyStr(result.productPosit.productPositInfor)); //摘要
            $('#TotalArea').html(getEmptyStr(result.productPosit.prodpositTotalArea + " &nbsp ㎡")); //项目总建设面积
            $('#RentArea').html(getEmptyStr(result.productPosit.prodpositRentArea + " &nbsp ㎡")); //长租公寓及配套商业总可租赁面积
            $('#TotalConstructionArea').html(getEmptyStr(result.productPosit.prodpositTotalConstructionArea + " &nbsp ㎡")); //长租公寓总建设面积
            $('#TotalBedrooms').html(getEmptyStr(result.productPosit.prodpositTotalBedrooms + " &nbsp 间")); //卧室总间数 
            $('#CoWorkingArea').html(getEmptyStr(result.productPosit.prodpositCoWorkingArea + " &nbsp ㎡")); //联合办公总建设面积
            $('#CoWorkingNum').html(getEmptyStr(result.productPosit.prodpositCoWorkingNum + " &nbsp 个")); //总可租赁工位数   
            $('#BusinessTotalArea').html(getEmptyStr(result.productPosit.prodpositBusinessTotalArea + " &nbsp ㎡")); //配套商业总建设面积
            $('#BusinessRentArea').html(getEmptyStr(result.productPosit.prodpositBusinessRentArea + " &nbsp ㎡")); //配套商业总可租赁面积

            var prodpositDoorModelTypes = result.productPosit.prodpositDoorModelTypes;
            $('#DoorModelTypes').html(getEmptyStr(prodpositDoorModelTypes.join(' , '))); //户型种类

            //市场及客户分析
            $('#cusInfor').html(getEmptyStr(result.MarketCustomer.marketcusInfor));
            $('#RentPrice').html(getEmptyStr(result.MarketCustomer.marketcusRentPrice)); //租金定价
            $('#ContainsNoOtherFees').html(getEmptyStr(result.MarketCustomer.marketcusContainsNoOtherFees)); //不含在租金内的其他费用

            //项目基本信息
            $('#locsupInfor').html(getEmptyStr(result.LocAndSupp.locsupInfor));

            //项目条件
            $('#TenantRent').html(getEmptyStr(result.prjConditions.prjcTenantRent + " &nbsp 元/平米·天")); //承租租金
            $('#GetForm').html(getEmptyStr(result.prjConditions.prjcGetForm)); //获取形式
            $('#YearsLease').html(getEmptyStr(result.prjConditions.prjcYearsLease + " &nbsp 年")); //冠寓租赁年限
            $('#TotalModifiedRoom').html(getEmptyStr(result.prjConditions.prodpositTotalModifiedRoom + " &nbsp 间")); //改造后可租赁房间数
            $('#OtherDescription').html(getEmptyStr(result.prjConditions.prjcOtherDescription)); //其他描述
            $('#Elevator').html(getEmptyStr(result.prjConditions.prjcElevator)); //电梯
            $('#NatureOfLand').html(getEmptyStr(result.prjConditions.prjcNatureOfLand.join(' , '))); //用地性质

            var prjcAir = result.prjConditions.prjcAir; //空调
            $('#prjcAir').html(getEmptyStr(prjcAir.join(' , ')));
            /*for(var i=0; i< prjcAir.length; i++){
            	$('#prjcAir').html(getEmptyStr(prjcAir[i]));
            }*/

            $('#PowerNeed').html(getEmptyStr(result.prjConditions.prjcWaterNeed)); //供水是否需要扩容
            $('#WaterNeed').html(getEmptyStr(result.prjConditions.prjcPowerNeed)); //供电是否需要增容
            $('#OutsideWindowNeed').html(getEmptyStr(result.prjConditions.prjcOutsideWindowNeed)); //外窗更换需求

            var Structural = result.prjConditions.prjcStructural; //结构加固
            $('#Structural').html(getEmptyStr(Structural.join(' , ')));
            /*for(var i=0; i< Structural.length; i++){
            	$('#Structural').html(getEmptyStr(Structural[i]));
            }*/

            $('#BuildingBasicInfor').html(getEmptyStr(result.prjConditions.prjcBuildingBasicInfor + " &nbsp栋")); //承租楼栋数

            var AdverseFactors = result.prjConditions.prjcAdverseFactors; //不利因素
            $('#AdverseFactors').html(getEmptyStr(AdverseFactors.join(' , ')));
            /*for(var i=0; i< AdverseFactors.length; i++){
            	
            }*/


            $('#projectInfo').html(getEmptyStr(result.prjConditions.lightProjectBaseInfo.projectInfo)); //项目简介
            $('#rentContractArea').html(getEmptyStr(getEmptyStr(result.prjConditions.lightProjectBaseInfo.rentContractArea) + " &nbsp ㎡")); //承租合同产权面积
            $('#address').html(getEmptyStr(result.prjConditions.lightProjectBaseInfo.address)); //项目地址
            $('#name').html(getEmptyStr(result.prjConditions.lightProjectBaseInfo.name)); //项目名称


            $('#headerName').html(getEmptyStr(result.name));

            var img = $('.imgurl');
            var imgTab = $('.imgurlTab');
            var imges = result.LocAndSupp.locsupInforPics;
            var status1 = true;
            var status2 = true;
            for (var j = 0; j < imges.length; j++) {
              if (imges[j].picTag == 1) {
                status1 = false;
                var imgUrlList = imges[j].picList; //.picUrl;
                var imgUrls = '';
                for (var i = 0; i < imgUrlList.length; i++) {

                  imgUrls += '<li><img class="imgItem" src="' + imgUrlList[i].picUrl + '" /><span style="display:none">'+imgUrlList[i].remark+'</span></li>';
                }
                img.append(imgUrls);

                $('.pic-container').mouseenter(function () {
                  var len = $(this).find('.pictures:visible li').length;
                  if (len > 0 && len<=1) {
                    $('.item-information').show();
                  } else if (len > 1) {
                    var l = $('.pictures:visible').position().left;
                    var w = $('.pictures:visible li').width();                    
                    if(l == 0){
                      $('.map-pictures .btn-prev').hide();
                      $('.map-pictures .btn-next').show();
                    } else if(l <=  -w * (len-1)){
                      $('.map-pictures .btn-next').hide();
                      $('.map-pictures .btn-prev').show();
                    } else {
                      $('.map-pictures .btn-next').show();
                      $('.map-pictures .btn-prev').show();
                    }
                      
                    $('.item-information').show();
                    
                   
                  }

                }).mouseleave(function () {
                  $('.map-pictures .btn').hide();
                  $('.item-information').hide();
                })

                //$('.map-pictures .btn').css('display','block');
                $('.pictures .imgItem').mouseenter(function(){
                  $(".item-information").html($(this).next().text());
                })
                

              }

              if (imges[j].picTag == 2) {
                status2 = false;
                var imgUrlListTab = imges[j].picList //[0].picList.picUrl;
                var imgUrlsTab = '';
                
                for (var i = 0; i < imgUrlListTab.length; i++) {
                  imgUrlsTab += '<li><img class="imgItem" src="' + imgUrlListTab[i].picUrl + '" /><span style="display:none">'+imgUrlListTab[i].remark+'</span></li>';
                }
                imgTab.append(imgUrlsTab);

                //img.eq(0).attr('str',imgUrl);
                //img.eq(1).attr('str',imgUrl1);
                // $('.map-pictures .btn').show();

              }



            }


            if (status1) {
              $('.map-pictures .btn').hide();
              $('#item-information').hide();
              img.css('display', 'block').append('<li><img class="imgItem" src="light/images/Group3@1x.png" /></li>');
            }
            if (status2) {
              $('.map-pictures .btn').hide();
              $('#item-information').hide();

              imgTab.css('display', 'block').append('<li><img class="imgItem" src="light/images/Group3@1x.png" /></li>');
            }

            imgSrc();

            var img2 = $('.AppImgurl');
            if (result.MarketCustomer.marketcusPic.length == 0) {

              $('.map-pictures2 .btn').hide();
              $('#item-information2').hide();
              img2.css('display', 'block').append('<li><img class="imgItem" src="light/images/Group3@1x.png" /></li>');

            } else {
              var imgUrlList2 = result.MarketCustomer.marketcusPic //[0].picList.picUrl;
              var imgUrls2 = '';
              var isTrue = true;
              for (var i = 0; i < imgUrlList2.length; i++) {
                if(imgUrlList2[i].picTag == 3){
                  isTrue = false;
                  for(var j = 0;j<imgUrlList2[i].picList.length;j++){
                    imgUrls2 += '<li><img class="imgItem" src="' + imgUrlList2[i].picList[j].picUrl + '" /><span style="display:none">'+imgUrlList2[i].picList[j].remark+'</span></li>';
                  }
                }
              }
              if(isTrue){
                $('.map-pictures2 .btn').hide();
                $('#item-information2').hide();
                img2.css('display', 'block').append('<li><img class="imgItem" src="light/images/Group3@1x.png" /></li>');
              }
              img2.append(imgUrls2);

              $('.pic-container2').mouseenter(function () {
                if ($(this).find('.pictures2:visible li').length = 1) {
                  $('.map-pictures2 .btn').hide();
                  $('.item-information2').show();
                }

                var len = $(this).find('.pictures2:visible li').length;
                if (len > 0 && len<=1) {
                  $('.item-information2').show();
                } else if (len > 1) {
                  var l = $('.pictures2:visible').position().left;
                  var w = $('.pictures2:visible li').width();
                  if(l == 0){
                    $('.map-pictures2 .btn-prev2').hide();
                    $('.map-pictures2 .btn-next2').show();
                  } else if(l <=  -w * (len-1)){
                    $('.map-pictures2 .btn-next2').hide();
                    $('.map-pictures2 .btn-prev2').show();
                  } else {
                     $('.map-pictures2 .btn-next2').show();
                     $('.map-pictures2 .btn-prev2').show();
                  }
                    
                  $('.item-information2').show();
                  
                 
                }


              }).mouseleave(function () {
                $('.map-pictures2 .btn').hide();
                $('.item-information2').hide();
              })

             
              
            }
            $('.pictures2 .imgItem').mouseenter(function(){
              $(".item-information2").html($(this).next().text());
            })
            imgTwo();

            $('#default_').click();

          }
        }
      }
    },
    error: function (error) {
      // console.log(error);
    },
    complete: function () {
      $.ajax({
        url: url_interface_business,
        type: 'POST', //GET
        /*data: JSON.stringify({
          flowNo:flowNo,  //正式使用
          // flowNo: "8dc2a3d3-ff2e-4b35-9993-a99995cc5e1e", //测试
          systemNo: "LONGCITY",
          usercode: userCode,
          businessType:'LCHAP'
        }),*/
        headers: {
          'token': token,
          'version': "v1",
          'platform': "ios",
          "Content-Type": "application/json;charset=UTF-8"
        },
        data: JSON.stringify({
          instanceId: flowNo, //正式使用
          // flowNo: "8dc2a3d3-ff2e-4b35-9993-a99995cc5e1e", //测试
          flag: '1',
          assetType: '1',
          userCode: userCode
        }),
        contentType: 'application/json;charset=UTF-8',
        dataType: 'json', //返回的数据格式：json/xml/html/script/jsonp/text
        beforeSend: function (xhr) {
          $('#loading').show();
          $('#pc').hide();
        },
        success: function (data) {
          var bznsData = data.result.functional;
          flowInterface(token, bznsData, userCode);
        }
      });

    }
  });
}

//tab切换轮播；
function imgSrc() {
  var _index = 0;
  var lis = $('.pictures:visible').find('li');
  var imgWidth = $('.pictures li').width();
  $('.box-items').on('click', 'span', function () {
    $(this).addClass('active').siblings().removeClass();
    var idx = $(this).index();
    $('.pictures').eq(idx).show().siblings().hide();
  })
  $('.pictures').show();
  //$('.pictures').not($('.pictures').eq($('.box-items span.active')).index()).hide();
  //---------------------

  var imgUrl = $('.pictures ');

  //---------------------

  $('.pictures').css('width', lis.length * imgWidth + 'px')
  $('.btn-next').on("click", function () {
    //右箭头
    _index++;
    if (_index > lis.length - 1) {
      _index = lis.length - 1
    }
    if (_index == lis.length - 1){
      $(this).hide();
      $('.btn-prev').show();
    } else{
      $('.btn-prev').show();
      $('.btn-next').show();
    }
    selectPic(_index);
  })
  $('.btn-prev').on("click", function () {
    //左箭头
    _index--;
    if (_index < 0) {
      _index = 0;
    }
    if (_index == 0){
      $(this).hide();
      $('.btn-next').show();
    } else{
      $('.btn-next').show();
      $('.btn-prev').show();
    }
    selectPic(_index);
  })

  function selectPic(num) {
    $(".pictures:visible").stop().animate({
      left: -num * imgWidth,
    }, 1000,function(){
      var theVal = $(".pictures:visible li").eq(num).find('span').text();
      $(".item-information").html(theVal);
    });
  }


}


function imgTwo() {
  var _index = 0;
  var lis = $('.pictures2').find('li');
  var imgWidth = $('.pictures2 li').width();
  $('.pictures2').show();
  //---------------------

  var imgUrl = $('.pictures2');

  //---------------------

  $('.pictures2').css('width', lis.length * imgWidth + 'px');
  $('.btn-next2').on("click", function () {
    //右箭头
    _index++;
    if (_index > lis.length - 1) {
      _index = lis.length - 1;
      $(this).hide();
    }
    if (_index == lis.length - 1){
      $(this).hide();
      $('.btn-prev2').show();
    } else{
      $('.btn-prev2').show();
      $('.btn-next').show();
    }
    selectPic(_index);
  });
  $('.btn-prev2').on("click", function () {
    //左箭头
    _index--;
    if (_index < 0) {
      _index = 0;
      $(this).hide();
    }
    if (_index == 0){
      $(this).hide();
      $('.btn-next2').show();
    } else{
      $('.btn-next2').show();
      $('.btn-prev2').show();
    }
    selectPic(_index);
  });

  function selectPic(num) {   
    $(".pictures2:visible").stop().animate({
      left: -num * imgWidth,
    }, 1000,function(){
      var theVal = $(".pictures2:visible li").eq(num).find('span').text();
      $(".item-information2").html(theVal);
    });
  }
}
function alert(e,src) {
  $("body").append('<div class="mark">' +
    '<div class="mark-information">' +
    '<div class="mark-message"><img src="'+src+'" alt="">' + e + '</div>' +
    '<div class="close">确定</div>' +
    '</div>' +
    '</div>');
  $(".close").click(function () {
    $(".mark").remove();
  });
}

function flowInterface(token, bznsData, userCode) {
  $.ajax({
    url: url_interface_flow,
    type: 'POST', //GET
    data: JSON.stringify({
      flowNo: flowNo, //正式使用
      systemNo: "LONGCITY",
    }),
    contentType: 'application/json;charset=UTF-8',
    dataType: 'json', //返回的数据格式：json/xml/html/script/jsonp/text
    beforeSend: function (xhr) {
      $('#loading').show();
      $('#pc').hide();
    },
    success: function (data) {
      $('#loading').hide();
      $('#pc').show();
      flowInterfaceWs(bznsData, data);
    },
    error: function (error) {
      // console.log(error);
    },
    complete: function () {
      //
      //地区地产总经理审批意见
      $('#approval205NodeAgreement').click(function () {
        // submit('9b9df522-f20e-42d1-9e89-9e5d4186cbf0', '8dc2a3d3-ff2e-4b35-9993-a99995cc5e1e', 'approval200Node', 1, 1, 'songdi');

        if ($('.texts').val() == '' || $.trim($('.texts').val()) == '') {
          alert('请在评审意见框内发表评审意见','./heavy/images/error.png');
          $('.options button').attr('disabled', false);
        } else {
          submit(toDoId, instanceId, nodeNo, 1, 1, userCode);

        }
      });
      $('#approval205NodeAgainst').click(function () {
        // submit('9b9df522-f20e-42d1-9e89-9e5d4186cbf0', '8dc2a3d3-ff2e-4b35-9993-a99995cc5e1e', 'approval200Node', 1, 3, 'songdi');

        if ($('.texts').val() == '' || $.trim($('.texts').val()) == '') {
          alert('请在评审意见框内发表评审意见','./heavy/images/error.png');
          $('.options button').attr('disabled', false);
        } else {
          submit(toDoId, instanceId, nodeNo, 1, 3, userCode);

        }
      });
      //地区冠寓发展部负责人审批意见
      $('#approval204NodeAgreement').click(function () {
        // submit('9b9df522-f20e-42d1-9e89-9e5d4186cbf0', '8dc2a3d3-ff2e-4b35-9993-a99995cc5e1e', 'approval201Node', 1, 1, 'songdi');

        if ($('.texts').val() == '' || $.trim($('.texts').val()) == '') {
          alert('请在评审意见框内发表评审意见','./heavy/images/error.png');
          $('.options button').attr('disabled', false);
        } else {
          submit(toDoId, instanceId, nodeNo, 1, 1, userCode);

        }
      });
      $('#approval204NodeAgainst').click(function () {
        // submit('9b9df522-f20e-42d1-9e89-9e5d4186cbf0', '8dc2a3d3-ff2e-4b35-9993-a99995cc5e1e', 'approval201Node', 1, 3, 'songdi');
        //  submit(toDoId, instanceId, workItemId, isNormal, operateType, userCode)

        if ($('.texts').val() == '' || $.trim($('.texts').val()) == '') {
          alert('请在评审意见框内发表评审意见','./heavy/images/error.png');
          $('.options button').attr('disabled', false);
        } else {
          submit(toDoId, instanceId, nodeNo, 1, 3, userCode);

        }

      });
      //投委会审批意见
      //投委会审批第一阶段
      $('#approval209NodeAgreement').click(function () {
        // submit('9b9df522-f20e-42d1-9e89-9e5d4186cbf0', '8dc2a3d3-ff2e-4b35-9993-a99995cc5e1e', 'approval201Node', 1, 1, 'songdi');

        if ($('.texts').val() == '' || $.trim($('.texts').val()) == '') {
          alert('请在评审意见框内发表评审意见','./heavy/images/error.png');
          $('.options button').attr('disabled', false);
        } else {
          submit(toDoId, instanceId, nodeNo, 1, 1, userCode);

        }
      });
      $('#approval209NodeAgainst').click(function () {
        // submit('9b9df522-f20e-42d1-9e89-9e5d4186cbf0', '8dc2a3d3-ff2e-4b35-9993-a99995cc5e1e', 'approval201Node', 1, 3, 'songdi');
        //  submit(toDoId, instanceId, workItemId, isNormal, operateType, userCode)

        if ($('.texts').val() == '' || $.trim($('.texts').val()) == '') {
          alert('请在评审意见框内发表评审意见','./heavy/images/error.png');
          $('.options button').attr('disabled', false);
        } else {
          submit(toDoId, instanceId, nodeNo, 1, 4, userCode);

        }

      });
      //投委会审批第二阶段
      $('#approval210NodeAgreement').click(function () {
        // submit('024350c4-7cfb-4864-ba8c-cdecf06ac100', '', 'approval205Node', 1, 1, 'songdi');

        if ($('.texts').val() == '' || $.trim($('.texts').val()) == '') {
          alert('请在评审意见框内发表评审意见','./heavy/images/error.png');
          $('.options button').attr('disabled', false);
        } else {
          submit(toDoId, instanceId, nodeNo, 1, 1, userCode);

        }
      });
      $('#approval210NodeAgainst').click(function () {
        // submit('024350c4-7cfb-4864-ba8c-cdecf06ac100', '', 'approval205Node', 1, 4, 'songdi');

        if ($('.texts').val() == '' || $.trim($('.texts').val()) == '') {
          alert('请在评审意见框内发表评审意见','./heavy/images/error.png');
          $('.options button').attr('disabled', false);
        } else {
          submit(toDoId, instanceId, nodeNo, 1, 4, userCode);

        }
      });
      $('#approval210NodeAgreementOr').click(function () {
        // submit('024350c4-7cfb-4864-ba8c-cdecf06ac100', '', 'approval205Node', 1, 2, 'songdi');
        if ($('.texts').val() == '' || $.trim($('.texts').val()) == '') {
          alert('请在评审意见框内发表评审意见','./heavy/images/error.png');
          $('.options button').attr('disabled', false);
        } else {
          submit(toDoId, instanceId, nodeNo, 1, 2, userCode);

        }
      });

      //专业
      $('#submit').click(function () {

        //var approval202NodeOrapproval203Node = 'approval202Node' || 'approval203Node';
        // submit('024350c4-7cfb-4864-ba8c-cdecf06ac100', '', approval202NodeOrapproval203Node, 1, 1, 'songdi');
        //submit(toDoId, instanceId, approval202NodeOrapproval203Node, 0, 1, userCode)

        if ($('.texts').val() == '' || $.trim($('.texts').val()) == '') {
          alert('请在会签意见框内发表评审意见','./heavy/images/error.png');
          $('.options button').attr('disabled', false);

        } else {
          //var approvalOrNode = 'approval200Node' || 'approval201Node' || 'approval202Node' || 'approval203Node' || 'approval206Node' || 'approval207Node' || 'approval208Node' || 'approval211Node';
          // submit('024350c4-7cfb-4864-ba8c-cdecf06ac100', '', approval202NodeOrapproval203Node, 1, 1, 'songdi');
          submit(toDoId, instanceId, nodeNo, 0, 1, userCode);
        }
      });
      //
    }
  });
}

function flowInterfaceWs(bznsData, data) {
  var jsonDataTitle = '';
  if (bznsData) {
    jsonDataTitle = bznsData;
  }
  var flowData = JSON.parse(data.data.flowData);
  var approve_record = flowData.approve_record.approve_record;
  var flowStatus = flowData.flow_data;
  var nodeList = flowData.flow_data.nodeList;
  var tabList = flowData.approve_record.approve_record;
  approvalWeb(nodeList, tabList, flowStatus);
  var nodeListStatus2 = [];
  for (var i = 0; i < nodeList.length; i++) {
    if (nodeList[i].nodeStatus === '2') {
      nodeListStatus2.push(nodeList[i]);
    }
  }
  if (nodeListStatus2.length > 0) {
    nodeNo = nodeListStatus2[0].nodeNo;
  }


  if (!nodeListStatus2 || nodeListStatus2.length == 0) {
    nodeListStatus2[0] = 'x';
    nodeListStatus2[0].nodeNo = "";
  }
  //
  var dz = '';
  if (workItemType == 'Approve'&&Mode == 'Work') {
  
      if (nodeListStatus2[0].nodeNo == 'approval205Node') {

        dz = '<div class="local-opinion">' +
          '<div class="opinion-title">' +
          '<strong></strong>' +
          '<span>地区地产总经理审批意见</span>' +
          '</div>' +
          '<div class="opinion-information">' +
          '<div class="information">' +
          '<textarea class="texts" placeholder="请填写意见" value=""  style="resize:none;display:block;border: none;" maxlength="1500"></textarea>' +
          '<div class="count">' +
          '<span class="number">0</span>' +
          '<span>/1500</span>' +
          '</div>' +
          '</div>' +
          '<div class="options">' +
          '<button class="agree active" id="approval205NodeAgreement">同意</button>' +
          '<button class="rejected" id="approval205NodeAgainst">驳回</button>' +
          '</div>' +
          '</div>' +
          '</div>';
      } else if (nodeListStatus2[0].nodeNo == 'approval204Node') {
        dz = '<div class="local-opinion">' +
          '<div class="opinion-title">' +
          '<strong></strong>' +
          '<span>地区冠寓发展部负责人审批意见</span>' +
          '</div>' +
          '<div class="opinion-information">' +
          '<div class="information">' +
          '<textarea class="texts" placeholder="请填写意见" value=""  style="resize:none;display:block;border: none;" maxlength="1500"></textarea>' +
          '<div class="count">' +
          '<span class="number">0</span>' +
          '<span>/1500</span>' +
          '</div>' +
          '</div>' +
          '<div class="options">' +
          '<button class="agree active" id="approval204NodeAgreement">同意</button>' +
          '<button class="rejected" id="approval204NodeAgainst">驳回</button>' +
          '</div>' +
          '</div>' +
          '</div>';
      } else if (nodeListStatus2[0].nodeNo == 'approval200Node' || nodeListStatus2[0].nodeNo == 'approval201Node' || nodeListStatus2[0].nodeNo == 'approval202Node' || nodeListStatus2[0].nodeNo == 'approval203Node' || nodeListStatus2[0].nodeNo == 'approval206Node' || nodeListStatus2[0].nodeNo == 'approval207Node' || nodeListStatus2[0].nodeNo == 'approval208Node' || nodeListStatus2[0].nodeNo == 'approval211Node') {

        var pic1 =
          '<div class="fileItem">' +
          '<input type="button" value=""  onclick="document.getElementById(\'PicInput1\').click();" />' +
          '<img id="imgPicInput1" src="light/images/pic2.png">' +
          '</div>' +
          '<div class="fileItem">' +
          '<input type="button" value=""  onclick="document.getElementById(\'PicInput2\').click();" />' +
          '<img id="imgPicInput2" src="light/images/pic2.png">' +
          '</div>' +
          '<div class="fileItem">' +
          '<input type="button" value=""  onclick="document.getElementById(\'PicInput3\').click();" />' +
          '<img id="imgPicInput3" src="light/images/pic2.png">' +
          '</div>' +
          '<div class="fileItem">' +
          '<input type="button" value=""  onclick="document.getElementById(\'PicInput4\').click();" />' +
          '<img id="imgPicInput4" src="light/images/pic2.png">' +
          '</div>';
        dz = '<div class=" professional-opinion">' +
          '<div class="opinion-title">' +
          '<strong></strong>' +
          '<span id="jsonDataTitle">会签意见</span>' +
          '</div>' +
          '<div class="opinion-information">' +
          '<div class="information-box">' +
          '<div class="information">' +
          '<textarea class="texts information-texts" placeholder="请填写意见" value=""  style="resize:none;display:block;border: none;" maxlength="1500"></textarea>' +
          '<div class="count">' +
          '<span class="number">0</span>' +
          '<span>/1500</span>' +
          '</div>' +
          '</div>' +
          '<div class="picture-box" id="picture-box1">' +
          pic1 +
          '</div>' +
          '</div>' +
          '<div class="options">' +
          '<button class="agree active" id="submit">提交</button>' +
          '</div>' +
          '</div>' +
          '</div>' +
          '</div>' +
          '<input id="filess" style="display: none">';
      } else if (nodeListStatus2[0].nodeNo == 'approval210Node') {
        dz = '<div class="local-opinion">' +
          '<div class="opinion-title">' +
          '<strong></strong>' +
          '<span>投委会审批意见</span>' +
          '</div>' +
          '<div class="opinion-information">' +
          '<div class="information">' +
          '<textarea class="texts" placeholder="请填写意见" value=""  style="resize:none;display:block;border: none;" maxlength="1500"></textarea>' +
          '<div class="count">' +
          '<span class="number">0</span>' +
          '<span>/1500</span>' +
          '</div>' +
          '</div>' +
          '<div class="options">' +
          '<button class="agree active" id="approval210NodeAgreement">同意</button>' +
          '<button class="conditions" id="approval210NodeAgreementOr">有条件通过</button>' +
          '<button class="rejected" id="approval210NodeAgainst">否决</button>' +
          '</div>' +
          '</div>' +
          '</div>';


      } else if (nodeListStatus2[0].nodeNo == 'approval209Node') {
        dz = '<div class="local-opinion">' +
          '<div class="opinion-title">' +
          '<strong></strong>' +
          '<span>投委会审批意见</span>' +
          '</div>' +
          '<div class="opinion-information">' +
          '<div class="information">' +
          '<textarea class="texts" placeholder="请填写意见" value=""  style="resize:none;display:block;border: none;" maxlength="1500"></textarea>' +
          '<div class="count">' +
          '<span class="number">0</span>' +
          '<span>/1500</span>' +
          '</div>' +
          '</div>' +
          '<div class="options">' +
          '<button class="agree active" id="approval209NodeAgreement">同意</button>' +
          '<button class="rejected" id="approval209NodeAgainst">否决</button>' +
          '</div>' +
          '</div>' +
          '</div>';


      } else {
        dz = '';
      }
   
  } else {
    dz = '';
  }
  if (dz == '') {
    $('.opinions').css('margin-bottom', 0);
    $('.record-body').css('height', '100%');
    $('#scrollRecord').css('overflow-y', 'visible');
  }
  $('.opinions').append(dz);
  
  // $('.texts').on('keyup', function () {
  //   var len = $(this).val().length,
  //     num = $(this).next().find('.number');
  //   if (len <= 1500) {
  //     num.text($(this).val().length);
  //   } else {
  //     num.text(1500);
  //     // num.text($(this).val().length).css({});
  //     // var txt = $(this).val().substr(0, 1500);
  //     // $(this).empty().val(txt);
  //     // $(this).blur();
  //     // alert("输入内容不能超过1500个字符");
  //   }
  // });
    var node = $(".texts");
    var cpLock = false;
    node.on('compositionstart', function(){
        cpLock = true;       
    });
    node.on('compositionend', function(){
        cpLock = false;      
        calNum(this);
    });
    node.on('input', function(){        
        calNum(this);
    });
    //字符串截断函数
    function calNum(domEle) {
        if(!cpLock) {
            var maxLen = 1500;
            var curtLen = domEle.value.length;
            if ( curtLen > maxLen ) {
                domEle.value = domEle.value.substring(0, maxLen);
                alert("输入内容不能超过1500个字符",'./heavy/images/error.png');
            }
            $(".number").text(domEle.value.length.toString());
        }
    } 

  var recordHtml = '';
  for (var j = 1; j < approve_record.length; j++) {

    var content = approve_record[j].approveContent;
    var approveContent;
    if (!content) {
      continue;
    } else {
      if (content.indexOf("流程自动跳过!") > 0) {
        continue;
      }
      approveContent = JSON.parse(content);
    }

    if (approveContent) {
      if (approveContent.isNormal === '1') {
        if (approveContent.opinion) {
          recordHtml += '<li class="work">' +
            '<p class="label">' + approve_record[j].approvePerson + '</p>' +
            '<span class="date">' + approve_record[j].approveTime + '</span>' +
            '<span class="circle"></span>' +
            '<div class="content-time">' +
            '<p class="text-time">' + approveContent.opinion + '</p>' +
            '</div>' +
            '</li>';
        }

      } else {
        var str1 = '';
        if(approveContent['obj1']){
          str1 += '<div>' +
          '<div class="content-time-content text-time">' + approveContent['obj1'].value + '</div>' +
          '<div>';
        if (approveContent['obj1'].imgs && approveContent['obj1'].imgs.length > 0) {
          for (var i = 0; i < approveContent['obj1'].imgs.length; i++) {
            if (!approveContent['obj1'].imgs[i] || approveContent['obj1'].imgs[i] === undefined ||
              approveContent['obj1'].imgs[i] == null || approveContent['obj1'].imgs[i] === 'undefined') {
              continue;
            }
            str1 += '<img src="' + approveContent['obj1'].imgs[i] + '" style="width:60px; height: 60px; margin-right: 10px;"/>';
          }

        }
        str1 += '</div>' +
          '</div>';

        recordHtml += '<li class="work">' +
          '<p class="label">' + approve_record[j].approvePerson + '</p>' +
          '<span class="date">' + approve_record[j].approveTime + '</span>' +
          '<span class="circle"></span>' +
          '<div class="content-time">' +
          '<div class="content-time-box">' + str1 + '</div>' +
          '</div>' +
          '</li>';
        }
        

      }
    }

  }
  if (recordHtml == '') {
    recordHtml = '<li style="color: #ccc;">暂无数据</li>';
    $('#timeline').css('border-left', 'none');
  }
  $('#timeline').append(recordHtml);


}




//解析url 查询参数
function GetQueryString() {
  var url = window.location.search;
  if (url.indexOf("?") != -1) {
    var str = url.substr(1);
    strs = str.split("&");
    var searchArr = {};
    var key = new Array(strs.length);
    var value = new Array(strs.length);
    for (i = 0; i < strs.length; i++) {
      key[i] = strs[i].split("=")[0]
      value[i] = decodeURIComponent(strs[i].split("=")[1]);
      // alert(key[i]+"="+value[i]);
      searchArr[key[i]] = value[i];
    }
  }
  return searchArr;

}


function submit(toDoId, instanceId, workItemId, isNormal, operateType, userCode) {


  $('.options button').attr('disabled', true);
  var statusName = '';
  if (operateType == 1) {
    statusName = '同意';
  } else if (operateType == 2) {
    statusName = '有条件通过';
  } else if (operateType == 3) {
    statusName = '驳回';
  } else if (operateType == 4) {
    statusName = '否决';
  }
  var data = {};
  var dataNew = {};

  if (isNormal == 1) {
    obj = $('.information .texts').val();
    data = {
      toDoId: toDoId,
      instanceId: instanceId,
      workItemId: workItemId,
      commentText: {
        isNormal: isNormal,
        statusName: statusName,
        obj1: {
          keyName: "市场状况",
          value: "",
          imgs: []
        },
        obj2: {
          keyName: "项目指标及产品",
          value: "",
          imgs: []
        },
        obj3: {
          keyName: "财务指标及商务合作条件",
          value: "",
          imgs: []
        },
        obj4: {
          keyName: "综述及其他",
          value: "",
          imgs: []
        },
        functional: '',
        opinion: obj
      },
      operateType: operateType,
      userCode: userCode
    };
  } else if (isNormal == 0) {

    function arrNew(arr) {
      var arr1 = [];
      for (var y = 0; y < arr.length; y++) {
        if (arr[y] == null || arr[y] == undefined ||
          arr[y] === 'undefined' || arr[y] == '') {
          continue;
        }
        arr1.push(arr[y]);
      }
      return arr1;
    }



    obj = {
      obj1: {
        keyName: '',
        value: $('.information-texts').eq(0).val(),
        imgs: arrNew(imgList1)
      }
    };
    data = {
      toDoId: toDoId,
      instanceId: instanceId,
      workItemId: workItemId,
      commentText: {
        isNormal: isNormal,
        statusName: statusName,
        obj1: {
          keyName: obj.obj1.keyName,
          value: obj.obj1.value,
          imgs: obj.obj1.imgs
        },
        functional: '',
        opinion: ''
      },
      operateType: operateType,
      userCode: userCode
    };
  }
  $.ajax({
    url: url_bpmWorkFlow,
    type: 'POST', //GET
    headers: {
      'token': token,
      'version': "v1",
      'platform': "ios",
      "Content-Type": "application/json;charset=UTF-8"
    },
    data: JSON.stringify(data),
    //contentType: 'application/json;charset=UTF-8',
    dataType: 'json', //返回的数据格式：json/xml/html/script/jsonp/text
    beforeSend: function (xhr) {
      // console.log('发送前');
    },
    success: function (data) {
      if (data.code == 200) {
        var str = '<div class="mark">' +
          '<div class="mark-information">' +
          '<div class="mark-message"><img src="./heavy/images/right.png" alt="">恭喜您，您的意见已成功提交！</div>' +
          '<div class="close">关闭</div>' +
          '</div>' +
          '</div>';
        $('body').append(str);

        $(this).css({
          'background': '#ccc',
          'color': '#999',
          'border-color': '#ccc'
        });
        //$('.options button').attr('disabled',true);
        $('body').on('click', '.mark', function () {
          $(this).remove();
          window.location.href = window.location.href.replace('Work', 'View');
        }).on('click', '.close', function () {
          $('.mark').remove();
          window.location.href = window.location.href.replace('Work', 'View');
        });
      }

    },
    error: function (error) {
      var str = '<div class="mark">' +
        '<div class="mark-information">' +
        '<div class="mark-message"><img src="./heavy/images/error.png" alt="">很抱歉，您的意见未能成功提交，请核对后再次进行操作。</div>' +
        '<div class="close">关闭</div>' +
        '</div>' +
        '</div>';
      $('body').append(str);
      $('.options button').attr('disabled', false);
      $('body').on('click', '.mark', function () {
        $(this).remove();

      }).on('click', '.close', function () {
        $('.mark').remove();
      });
    }
  });
}